/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccwishlistbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SaccwishlistbackofficeConstants extends GeneratedSaccwishlistbackofficeConstants
{
	public static final String EXTENSIONNAME = "saccwishlistbackoffice";

	private SaccwishlistbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
