# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import essential data for the Accelerator
#

# Language
$lang=en

# Languages
UPDATE Language;isocode[unique=true];name[lang=$lang]
 ;en;"English"
 ;ar;"Arabic"

# Currencies
UPDATE Currency;isocode[unique=true];name[lang=$lang]
 ;SAR;Saudi Riyal
 ;AED;Emarati Dirham
 ;USD;US Dollar
 


# Titles
UPDATE Title;code[unique=true];name[lang=$lang]
 ;Miss.;"Miss."
 ;Mr.;"Mr."
 ;Mrs.;"Mrs."

UPDATE StandardPaymentMode;code[unique=true]	;name[lang=en]			
								 ;card				;Card		  								
								 ;cod				;Cash On Delivery							
								 ;continue			;CONTINUE									
								 ;pis				;PAY IN-STORE   
								 
# Credit / Debit Cards
UPDATE CreditCardType;code[unique=true];name[lang=$lang]
 ;amex;"American Express"
 ;diners;"Diner's Club"
 ;maestro;"Maestro"
 ;master;"Mastercard"
 ;mastercard_eurocard;"Mastercard/Eurocard"
 ;switch;"Switch"
 ;visa;"Visa"
 ;mada;"Mada"

# DistanceUnits for Storelocator 
UPDATE DistanceUnit;code[unique=true];name[lang=$lang]
 ;km;"km"
 ;miles;"miles"

# MediaFormats
UPDATE MediaFormat;qualifier[unique=true];name[lang=$lang];
 ;1200Wx1200H;"1200Wx1200H"
 ;300Wx300H;"300Wx300H"
 ;30Wx30H;"30Wx30H"
 ;365Wx246H;"365Wx246H"
 ;515Wx515H;"515Wx515H"
 ;65Wx65H;"65Wx65H"
 ;96Wx96H;"96Wx96H"



## User Tax Groups
UPDATE UserTaxGroup;code[unique=true];name[lang=$lang]
 ;sk-sa-taxes;"Skysales SA Taxes"

## Product Tax Groups
UPDATE ProductTaxGroup;code[unique=true];name[lang=$lang]
 ;sk-sa-vat-full;"Skysales SA Full Tax rate"
 ;sk-sa-vat-free;"Skysales SA Free Tax"
 
## Tax
UPDATE Tax;code[unique=true];name[lang=$lang]
 ;sk-sa-vat-full;"Skysales SA VAT"
 ;sk-sa-vat-free;"Skysales SA No VAT"

# Consginment statuses
UPDATE ConsignmentStatus;code[unique=true];name[lang=$lang]
 ;CANCELLED;"Cancelled"
 ;PICKPACK;"Pickpack"
 ;PICKUP_COMPLETE;"Pickup complete"
 ;READY;"Ready"
 ;READY_FOR_PICKUP;"Ready for pickup"
 ;SHIPPED;"Shipped"
 ;WAITING;"Waiting"
