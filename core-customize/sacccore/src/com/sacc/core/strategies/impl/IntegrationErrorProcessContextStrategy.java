/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.core.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.AbstractOrderProcessContextStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Optional;

import com.sacc.core.model.SendingErrorEmailProcessModel;


/**
 * @author amjad.shati@erabia.com
 */
public class IntegrationErrorProcessContextStrategy extends AbstractOrderProcessContextStrategy
{
	@Override
	protected Optional<AbstractOrderModel> getOrderModel(final BusinessProcessModel businessProcessModel)
	{
		return Optional.of(businessProcessModel).filter(businessProcess -> businessProcess instanceof SendingErrorEmailProcessModel)
				.map(businessProcess -> ((SendingErrorEmailProcessModel) businessProcess).getOrder());
	}
}
