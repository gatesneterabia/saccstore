package com.sacc.core.event;

import com.sacc.core.enums.IntegrationProvider;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;



/**
 *@author abu-muhasien
 */
public class SendErrorEmailEvent extends AbstractEvent
{
	private IntegrationProvider integrationProvider;
	private AbstractOrderModel abstractOrder;
	private String errorMsg;
	
	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process the process
	 */
	public SendErrorEmailEvent(IntegrationProvider integrationProvider, AbstractOrderModel abstractOrder, String errorMsg)
	{
		super();
		this.integrationProvider=integrationProvider;
		this.abstractOrder=abstractOrder;
		this.errorMsg=errorMsg;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg()
	{
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg)
	{
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the abstractOrder
	 */
	public AbstractOrderModel getAbstractOrder()
	{
		return abstractOrder;
	}
	/**
	 * @param abstractOrder the abstractOrder to set
	 */
	public void setAbstractOrder(AbstractOrderModel abstractOrder)
	{
		this.abstractOrder = abstractOrder;
	}
	/**
	 * @return the integrationProvider
	 */
	public IntegrationProvider getIntegrationProvider()
	{
		return integrationProvider;
	}
	/**
	 * @param integrationProvider the integrationProvider to set
	 */
	public void setIntegrationProvider(IntegrationProvider integrationProvider)
	{
		this.integrationProvider = integrationProvider;
	}
}