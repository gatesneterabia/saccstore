/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccotpbackoffice.services;

/**
 * Hello World SaccotpbackofficeService
 */
public class SaccotpbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
