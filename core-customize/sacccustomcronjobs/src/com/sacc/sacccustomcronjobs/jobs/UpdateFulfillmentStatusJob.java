/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomcronjobs.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.sacccustomcronjobs.model.OrderProcessingCronJobModel;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.service.CustomConsignmentService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateFulfillmentStatusJob extends AbstractJobPerformable<OrderProcessingCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(UpdateFulfillmentStatusJob.class);
	private static final String CRONJOB_FINISHED = "UpdateFulfillmentStatusJob is Finished ...";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public PerformResult perform(final OrderProcessingCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");

		final List<ConsignmentModel> consignments = customConsignmentService
				.getConsignmentsByNotStatus(ConsignmentStatus.DELIVERY_COMPLETED);


		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		consignments.stream().filter(Objects::nonNull).forEach(c -> checkAndChangeConsignmentStatus(c));
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
	{
		try
		{
			final FulfillmentProviderType type = consignment.getCarrierDetails().getFulfillmentProviderType();
			if (type == null)
			{
				LOG.error("Empty type for consignment: " + consignment.getCode());
				return;
			}
			final Optional<ConsignmentStatus> optionalStatus = fulfillmentContext.updateStatus(consignment, type);
			if (optionalStatus.isEmpty())
			{
				LOG.error("Empty status for consignment: " + consignment.getCode());
				return;
			}
			if (ConsignmentStatus.DELIVERY_COMPLETED.equals(optionalStatus.get()))
			{
				saccERPService.updateSaleOrderDeliveredStatus(consignment);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(consignment.getOrder().getStore()),
					consignment.getOrder(), e.getCause() != null ? e.getCause().getMessage() : e.getMessage()));
			return;
		}
	}

	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SMSAFULFILLMENTPROVIDER":
				return IntegrationProvider.SMSA;
			case "SAEEFULFILLMENTPROVIDER":
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


}