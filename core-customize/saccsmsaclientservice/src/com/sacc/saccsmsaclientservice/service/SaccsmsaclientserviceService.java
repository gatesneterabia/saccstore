/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccsmsaclientservice.service;

public interface SaccsmsaclientserviceService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
