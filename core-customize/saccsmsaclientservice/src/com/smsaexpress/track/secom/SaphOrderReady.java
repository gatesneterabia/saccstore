/**
 * SaphOrderReady.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class SaphOrderReady  implements java.io.Serializable {
    private java.lang.String passKey;

    private java.lang.String refId;

    private com.smsaexpress.track.secom.OrderLineItem[] orderLineItems;

    public SaphOrderReady() {
    }

    public SaphOrderReady(
           java.lang.String passKey,
           java.lang.String refId,
           com.smsaexpress.track.secom.OrderLineItem[] orderLineItems) {
           this.passKey = passKey;
           this.refId = refId;
           this.orderLineItems = orderLineItems;
    }


    /**
     * Gets the passKey value for this SaphOrderReady.
     * 
     * @return passKey
     */
    public java.lang.String getPassKey() {
        return passKey;
    }


    /**
     * Sets the passKey value for this SaphOrderReady.
     * 
     * @param passKey
     */
    public void setPassKey(java.lang.String passKey) {
        this.passKey = passKey;
    }


    /**
     * Gets the refId value for this SaphOrderReady.
     * 
     * @return refId
     */
    public java.lang.String getRefId() {
        return refId;
    }


    /**
     * Sets the refId value for this SaphOrderReady.
     * 
     * @param refId
     */
    public void setRefId(java.lang.String refId) {
        this.refId = refId;
    }


    /**
     * Gets the orderLineItems value for this SaphOrderReady.
     * 
     * @return orderLineItems
     */
    public com.smsaexpress.track.secom.OrderLineItem[] getOrderLineItems() {
        return orderLineItems;
    }


    /**
     * Sets the orderLineItems value for this SaphOrderReady.
     * 
     * @param orderLineItems
     */
    public void setOrderLineItems(com.smsaexpress.track.secom.OrderLineItem[] orderLineItems) {
        this.orderLineItems = orderLineItems;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaphOrderReady)) return false;
        SaphOrderReady other = (SaphOrderReady) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.passKey==null && other.getPassKey()==null) || 
             (this.passKey!=null &&
              this.passKey.equals(other.getPassKey()))) &&
            ((this.refId==null && other.getRefId()==null) || 
             (this.refId!=null &&
              this.refId.equals(other.getRefId()))) &&
            ((this.orderLineItems==null && other.getOrderLineItems()==null) || 
             (this.orderLineItems!=null &&
              java.util.Arrays.equals(this.orderLineItems, other.getOrderLineItems())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPassKey() != null) {
            _hashCode += getPassKey().hashCode();
        }
        if (getRefId() != null) {
            _hashCode += getRefId().hashCode();
        }
        if (getOrderLineItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderLineItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderLineItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaphOrderReady.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">saphOrderReady"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "passKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "refId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderLineItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "orderLineItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "OrderLineItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "OrderLineItem"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
