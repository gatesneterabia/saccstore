/**
 * SMSAWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public interface SMSAWebService extends javax.xml.rpc.Service {

/**
 * SMSA eCommerce Web API
 */
    public java.lang.String getSMSAWebServiceSoapAddress();

    public com.smsaexpress.track.secom.SMSAWebServiceSoap getSMSAWebServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.smsaexpress.track.secom.SMSAWebServiceSoap getSMSAWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
