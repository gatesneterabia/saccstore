/**
 * SaphOrderReadyResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class SaphOrderReadyResponse  implements java.io.Serializable {
    private java.lang.String saphOrderReadyResult;

    public SaphOrderReadyResponse() {
    }

    public SaphOrderReadyResponse(
           java.lang.String saphOrderReadyResult) {
           this.saphOrderReadyResult = saphOrderReadyResult;
    }


    /**
     * Gets the saphOrderReadyResult value for this SaphOrderReadyResponse.
     * 
     * @return saphOrderReadyResult
     */
    public java.lang.String getSaphOrderReadyResult() {
        return saphOrderReadyResult;
    }


    /**
     * Sets the saphOrderReadyResult value for this SaphOrderReadyResponse.
     * 
     * @param saphOrderReadyResult
     */
    public void setSaphOrderReadyResult(java.lang.String saphOrderReadyResult) {
        this.saphOrderReadyResult = saphOrderReadyResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaphOrderReadyResponse)) return false;
        SaphOrderReadyResponse other = (SaphOrderReadyResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.saphOrderReadyResult==null && other.getSaphOrderReadyResult()==null) || 
             (this.saphOrderReadyResult!=null &&
              this.saphOrderReadyResult.equals(other.getSaphOrderReadyResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSaphOrderReadyResult() != null) {
            _hashCode += getSaphOrderReadyResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaphOrderReadyResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">saphOrderReadyResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saphOrderReadyResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "saphOrderReadyResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
