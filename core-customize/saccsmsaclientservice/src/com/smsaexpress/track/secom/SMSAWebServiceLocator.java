/**
 * SMSAWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class SMSAWebServiceLocator extends org.apache.axis.client.Service implements com.smsaexpress.track.secom.SMSAWebService {

/**
 * SMSA eCommerce Web API
 */

    public SMSAWebServiceLocator() {
    }


    public SMSAWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SMSAWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SMSAWebServiceSoap
    private java.lang.String SMSAWebServiceSoap_address = "http://track.smsaexpress.com/SeCom/SMSAwebService.asmx";

    public java.lang.String getSMSAWebServiceSoapAddress() {
        return SMSAWebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SMSAWebServiceSoapWSDDServiceName = "SMSAWebServiceSoap";

    public java.lang.String getSMSAWebServiceSoapWSDDServiceName() {
        return SMSAWebServiceSoapWSDDServiceName;
    }

    public void setSMSAWebServiceSoapWSDDServiceName(java.lang.String name) {
        SMSAWebServiceSoapWSDDServiceName = name;
    }

    public com.smsaexpress.track.secom.SMSAWebServiceSoap getSMSAWebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SMSAWebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSMSAWebServiceSoap(endpoint);
    }

    public com.smsaexpress.track.secom.SMSAWebServiceSoap getSMSAWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.smsaexpress.track.secom.SMSAWebServiceSoapStub _stub = new com.smsaexpress.track.secom.SMSAWebServiceSoapStub(portAddress, this);
            _stub.setPortName(getSMSAWebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSMSAWebServiceSoapEndpointAddress(java.lang.String address) {
        SMSAWebServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.smsaexpress.track.secom.SMSAWebServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.smsaexpress.track.secom.SMSAWebServiceSoapStub _stub = new com.smsaexpress.track.secom.SMSAWebServiceSoapStub(new java.net.URL(SMSAWebServiceSoap_address), this);
                _stub.setPortName(getSMSAWebServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SMSAWebServiceSoap".equals(inputPortName)) {
            return getSMSAWebServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "SMSAWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "SMSAWebServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SMSAWebServiceSoap".equals(portName)) {
            setSMSAWebServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
