/**
 * DeliveredShipmentDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class DeliveredShipmentDetails  implements java.io.Serializable {
    private java.lang.String awbNo;

    private java.lang.String refNo;

    private java.lang.String shipDate;

    private java.lang.String cName;

    private java.lang.String delvTo;

    private java.lang.String delvDate;

    private java.lang.String cMobile;

    private java.lang.String codAmt;

    private java.lang.String itemDesc;

    public DeliveredShipmentDetails() {
    }

    public DeliveredShipmentDetails(
           java.lang.String awbNo,
           java.lang.String refNo,
           java.lang.String shipDate,
           java.lang.String cName,
           java.lang.String delvTo,
           java.lang.String delvDate,
           java.lang.String cMobile,
           java.lang.String codAmt,
           java.lang.String itemDesc) {
           this.awbNo = awbNo;
           this.refNo = refNo;
           this.shipDate = shipDate;
           this.cName = cName;
           this.delvTo = delvTo;
           this.delvDate = delvDate;
           this.cMobile = cMobile;
           this.codAmt = codAmt;
           this.itemDesc = itemDesc;
    }


    /**
     * Gets the awbNo value for this DeliveredShipmentDetails.
     * 
     * @return awbNo
     */
    public java.lang.String getAwbNo() {
        return awbNo;
    }


    /**
     * Sets the awbNo value for this DeliveredShipmentDetails.
     * 
     * @param awbNo
     */
    public void setAwbNo(java.lang.String awbNo) {
        this.awbNo = awbNo;
    }


    /**
     * Gets the refNo value for this DeliveredShipmentDetails.
     * 
     * @return refNo
     */
    public java.lang.String getRefNo() {
        return refNo;
    }


    /**
     * Sets the refNo value for this DeliveredShipmentDetails.
     * 
     * @param refNo
     */
    public void setRefNo(java.lang.String refNo) {
        this.refNo = refNo;
    }


    /**
     * Gets the shipDate value for this DeliveredShipmentDetails.
     * 
     * @return shipDate
     */
    public java.lang.String getShipDate() {
        return shipDate;
    }


    /**
     * Sets the shipDate value for this DeliveredShipmentDetails.
     * 
     * @param shipDate
     */
    public void setShipDate(java.lang.String shipDate) {
        this.shipDate = shipDate;
    }


    /**
     * Gets the cName value for this DeliveredShipmentDetails.
     * 
     * @return cName
     */
    public java.lang.String getCName() {
        return cName;
    }


    /**
     * Sets the cName value for this DeliveredShipmentDetails.
     * 
     * @param cName
     */
    public void setCName(java.lang.String cName) {
        this.cName = cName;
    }


    /**
     * Gets the delvTo value for this DeliveredShipmentDetails.
     * 
     * @return delvTo
     */
    public java.lang.String getDelvTo() {
        return delvTo;
    }


    /**
     * Sets the delvTo value for this DeliveredShipmentDetails.
     * 
     * @param delvTo
     */
    public void setDelvTo(java.lang.String delvTo) {
        this.delvTo = delvTo;
    }


    /**
     * Gets the delvDate value for this DeliveredShipmentDetails.
     * 
     * @return delvDate
     */
    public java.lang.String getDelvDate() {
        return delvDate;
    }


    /**
     * Sets the delvDate value for this DeliveredShipmentDetails.
     * 
     * @param delvDate
     */
    public void setDelvDate(java.lang.String delvDate) {
        this.delvDate = delvDate;
    }


    /**
     * Gets the cMobile value for this DeliveredShipmentDetails.
     * 
     * @return cMobile
     */
    public java.lang.String getCMobile() {
        return cMobile;
    }


    /**
     * Sets the cMobile value for this DeliveredShipmentDetails.
     * 
     * @param cMobile
     */
    public void setCMobile(java.lang.String cMobile) {
        this.cMobile = cMobile;
    }


    /**
     * Gets the codAmt value for this DeliveredShipmentDetails.
     * 
     * @return codAmt
     */
    public java.lang.String getCodAmt() {
        return codAmt;
    }


    /**
     * Sets the codAmt value for this DeliveredShipmentDetails.
     * 
     * @param codAmt
     */
    public void setCodAmt(java.lang.String codAmt) {
        this.codAmt = codAmt;
    }


    /**
     * Gets the itemDesc value for this DeliveredShipmentDetails.
     * 
     * @return itemDesc
     */
    public java.lang.String getItemDesc() {
        return itemDesc;
    }


    /**
     * Sets the itemDesc value for this DeliveredShipmentDetails.
     * 
     * @param itemDesc
     */
    public void setItemDesc(java.lang.String itemDesc) {
        this.itemDesc = itemDesc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeliveredShipmentDetails)) return false;
        DeliveredShipmentDetails other = (DeliveredShipmentDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.awbNo==null && other.getAwbNo()==null) || 
             (this.awbNo!=null &&
              this.awbNo.equals(other.getAwbNo()))) &&
            ((this.refNo==null && other.getRefNo()==null) || 
             (this.refNo!=null &&
              this.refNo.equals(other.getRefNo()))) &&
            ((this.shipDate==null && other.getShipDate()==null) || 
             (this.shipDate!=null &&
              this.shipDate.equals(other.getShipDate()))) &&
            ((this.cName==null && other.getCName()==null) || 
             (this.cName!=null &&
              this.cName.equals(other.getCName()))) &&
            ((this.delvTo==null && other.getDelvTo()==null) || 
             (this.delvTo!=null &&
              this.delvTo.equals(other.getDelvTo()))) &&
            ((this.delvDate==null && other.getDelvDate()==null) || 
             (this.delvDate!=null &&
              this.delvDate.equals(other.getDelvDate()))) &&
            ((this.cMobile==null && other.getCMobile()==null) || 
             (this.cMobile!=null &&
              this.cMobile.equals(other.getCMobile()))) &&
            ((this.codAmt==null && other.getCodAmt()==null) || 
             (this.codAmt!=null &&
              this.codAmt.equals(other.getCodAmt()))) &&
            ((this.itemDesc==null && other.getItemDesc()==null) || 
             (this.itemDesc!=null &&
              this.itemDesc.equals(other.getItemDesc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAwbNo() != null) {
            _hashCode += getAwbNo().hashCode();
        }
        if (getRefNo() != null) {
            _hashCode += getRefNo().hashCode();
        }
        if (getShipDate() != null) {
            _hashCode += getShipDate().hashCode();
        }
        if (getCName() != null) {
            _hashCode += getCName().hashCode();
        }
        if (getDelvTo() != null) {
            _hashCode += getDelvTo().hashCode();
        }
        if (getDelvDate() != null) {
            _hashCode += getDelvDate().hashCode();
        }
        if (getCMobile() != null) {
            _hashCode += getCMobile().hashCode();
        }
        if (getCodAmt() != null) {
            _hashCode += getCodAmt().hashCode();
        }
        if (getItemDesc() != null) {
            _hashCode += getItemDesc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeliveredShipmentDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "DeliveredShipmentDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("awbNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "awbNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "refNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "shipDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "cName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delvTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "delvTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delvDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "delvDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CMobile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "cMobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "codAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "itemDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
