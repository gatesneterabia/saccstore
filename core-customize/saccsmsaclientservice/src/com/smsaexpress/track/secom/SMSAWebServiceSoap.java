/**
 * SMSAWebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public interface SMSAWebServiceSoap extends java.rmi.Remote {

    /**
     * Create Shipment and get SMSA AWB Number
     */
    public java.lang.String addShipment(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc) throws java.rmi.RemoteException;

    /**
     * Create Shipment with Shipper Details and get SMSA AWB Number
     */
    public java.lang.String addShip(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String sName, java.lang.String sContact, java.lang.String sAddr1, java.lang.String sAddr2, java.lang.String sCity, java.lang.String sPhone, java.lang.String sCntry, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException;

    /**
     * Create Shipment with Shipper Details and get SMSA AWB Number
     * for Multi piece Shipments
     */
    public java.lang.String addShipMPS(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String sName, java.lang.String sContact, java.lang.String sAddr1, java.lang.String sAddr2, java.lang.String sCity, java.lang.String sPhone, java.lang.String sCntry, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException;

    /**
     * Ship to Origin after Delivery
     */
    public java.lang.String stoShipment(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Create Shipment with Delivery Details and get SMSA AWB Number
     */
    public java.lang.String addShipmentDelv(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException;

    /**
     * Get Tracking of Shipment by Air waybill Number
     */
    public com.smsaexpress.track.secom.GetTrackingResponseGetTrackingResult getTracking(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Tracking of Shipment with Reference Number by Air waybill
     * Number
     */
    public com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getTrackingwithRef(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get current Status of Shipment by Air waybill Number
     */
    public java.lang.String getStatus(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException;
    public java.lang.String saphOrderReady(java.lang.String passKey, java.lang.String refId, com.smsaexpress.track.secom.OrderLineItem[] orderLineItems) throws java.rmi.RemoteException;

    /**
     * Get Status of Shipment by Customer Reference Number
     */
    public java.lang.String getStatusByRef(java.lang.String refNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Tracking of Shipment by Air waybill Number
     */
    public com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getTrackingByRef(java.lang.String refNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get All the Shipment Updates for the customer shipments
     */
    public com.smsaexpress.track.secom.GetShipUpdatesResponseGetShipUpdatesResult getShipUpdates(int rowId, java.lang.String passKey) throws java.rmi.RemoteException;

    /**
     * Get Tracking of Shipment by Air waybill Number
     */
    public com.smsaexpress.track.secom.GetTrackResp getTrack(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get All the Shipment Updates for the customer shipments
     */
    public com.smsaexpress.track.secom.GetShipmentUpdatesResp getShipmentUpdates(int rowId, java.lang.String passKey) throws java.rmi.RemoteException;

    /**
     * Cancel a Shipment by Air waybill Number
     */
    public java.lang.String cancelShipment(java.lang.String awbNo, java.lang.String passkey, java.lang.String reas) throws java.rmi.RemoteException;

    /**
     * Get List of Cities for Retails list
     */
    public com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getRTLCities(java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Retails list by each city
     */
    public com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getRTLRetails(java.lang.String cityCode, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Retails list by each city
     */
    public com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getAllRetails(java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Retails list by each city with Working Hours
     */
    public com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getRTLRetailsTiming(java.lang.String cityCode, java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get Retails list by each city with Working Hours
     */
    public com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getAllRetailsTiming(java.lang.String passkey) throws java.rmi.RemoteException;

    /**
     * Get AWB Print in PDF
     */
    public byte[] getPDF(java.lang.String awbNo, java.lang.String passKey) throws java.rmi.RemoteException;

    /**
     * Get AWB Print in PDF
     */
    public byte[] getPDFSino(java.lang.String awbNo, java.lang.String passKey) throws java.rmi.RemoteException;

    /**
     * Get AWB Print in PDF
     */
    public byte[] getPDFBr(java.lang.String awbNo, java.lang.String passKey, java.lang.String forwrdr) throws java.rmi.RemoteException;

    /**
     * Get Delivered Shipment details during the mentioned dates
     */
    public com.smsaexpress.track.secom.DeliveredShipmentResponse getDeliveredShipments(java.lang.String passKey, java.lang.String fromDate, java.lang.String toDate) throws java.rmi.RemoteException;
}
