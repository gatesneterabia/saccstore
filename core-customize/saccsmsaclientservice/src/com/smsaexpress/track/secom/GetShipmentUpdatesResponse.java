/**
 * GetShipmentUpdatesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetShipmentUpdatesResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetShipmentUpdatesResp getShipmentUpdatesResult;

    public GetShipmentUpdatesResponse() {
    }

    public GetShipmentUpdatesResponse(
           com.smsaexpress.track.secom.GetShipmentUpdatesResp getShipmentUpdatesResult) {
           this.getShipmentUpdatesResult = getShipmentUpdatesResult;
    }


    /**
     * Gets the getShipmentUpdatesResult value for this GetShipmentUpdatesResponse.
     * 
     * @return getShipmentUpdatesResult
     */
    public com.smsaexpress.track.secom.GetShipmentUpdatesResp getGetShipmentUpdatesResult() {
        return getShipmentUpdatesResult;
    }


    /**
     * Sets the getShipmentUpdatesResult value for this GetShipmentUpdatesResponse.
     * 
     * @param getShipmentUpdatesResult
     */
    public void setGetShipmentUpdatesResult(com.smsaexpress.track.secom.GetShipmentUpdatesResp getShipmentUpdatesResult) {
        this.getShipmentUpdatesResult = getShipmentUpdatesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipmentUpdatesResponse)) return false;
        GetShipmentUpdatesResponse other = (GetShipmentUpdatesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getShipmentUpdatesResult==null && other.getGetShipmentUpdatesResult()==null) || 
             (this.getShipmentUpdatesResult!=null &&
              this.getShipmentUpdatesResult.equals(other.getGetShipmentUpdatesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetShipmentUpdatesResult() != null) {
            _hashCode += getGetShipmentUpdatesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipmentUpdatesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getShipmentUpdatesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getShipmentUpdatesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getShipmentUpdatesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getShipmentUpdatesResp"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
