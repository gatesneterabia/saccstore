/**
 * CancelShipment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class CancelShipment  implements java.io.Serializable {
    private java.lang.String awbNo;

    private java.lang.String passkey;

    private java.lang.String reas;

    public CancelShipment() {
    }

    public CancelShipment(
           java.lang.String awbNo,
           java.lang.String passkey,
           java.lang.String reas) {
           this.awbNo = awbNo;
           this.passkey = passkey;
           this.reas = reas;
    }


    /**
     * Gets the awbNo value for this CancelShipment.
     * 
     * @return awbNo
     */
    public java.lang.String getAwbNo() {
        return awbNo;
    }


    /**
     * Sets the awbNo value for this CancelShipment.
     * 
     * @param awbNo
     */
    public void setAwbNo(java.lang.String awbNo) {
        this.awbNo = awbNo;
    }


    /**
     * Gets the passkey value for this CancelShipment.
     * 
     * @return passkey
     */
    public java.lang.String getPasskey() {
        return passkey;
    }


    /**
     * Sets the passkey value for this CancelShipment.
     * 
     * @param passkey
     */
    public void setPasskey(java.lang.String passkey) {
        this.passkey = passkey;
    }


    /**
     * Gets the reas value for this CancelShipment.
     * 
     * @return reas
     */
    public java.lang.String getReas() {
        return reas;
    }


    /**
     * Sets the reas value for this CancelShipment.
     * 
     * @param reas
     */
    public void setReas(java.lang.String reas) {
        this.reas = reas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelShipment)) return false;
        CancelShipment other = (CancelShipment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.awbNo==null && other.getAwbNo()==null) || 
             (this.awbNo!=null &&
              this.awbNo.equals(other.getAwbNo()))) &&
            ((this.passkey==null && other.getPasskey()==null) || 
             (this.passkey!=null &&
              this.passkey.equals(other.getPasskey()))) &&
            ((this.reas==null && other.getReas()==null) || 
             (this.reas!=null &&
              this.reas.equals(other.getReas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAwbNo() != null) {
            _hashCode += getAwbNo().hashCode();
        }
        if (getPasskey() != null) {
            _hashCode += getPasskey().hashCode();
        }
        if (getReas() != null) {
            _hashCode += getReas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelShipment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">cancelShipment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("awbNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "awbNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passkey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "passkey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "reas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
