/**
 *
 */
package com.sacc.saccorderconfirmation.dao;

import java.util.List;

import com.sacc.saccorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailDao
{
	/**
	 * @param storeUid
	 * @return
	 */
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
