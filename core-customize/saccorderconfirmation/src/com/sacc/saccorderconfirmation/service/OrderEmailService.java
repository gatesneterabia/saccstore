/**
 *
 */
package com.sacc.saccorderconfirmation.service;

import java.util.List;

import com.sacc.saccorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailService
{
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
