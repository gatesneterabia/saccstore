/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.hybris.cockpitng.actions.ActionContext
 *  com.hybris.cockpitng.actions.ActionResult
 *  com.hybris.cockpitng.actions.CockpitAction
 *  de.hybris.platform.basecommerce.enums.ConsignmentStatus
 *  de.hybris.platform.ordersplitting.model.ConsignmentModel
 *  de.hybris.platform.servicelayer.config.ConfigurationService
 *  de.hybris.platform.warehousing.process.BusinessProcessException
 *  de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService
 *  de.hybris.platform.workflow.enums.WorkflowActionStatus
 *  de.hybris.platform.workflow.model.WorkflowActionModel
 *  javax.annotation.Resource
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 */
package com.sacc.saccfulfillmentbackoffice.actions;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.warehousing.process.BusinessProcessException;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.warehousingbackoffice.labels.strategy.ConsignmentPrintDocumentStrategy;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Objects;
public class CustomPrintPackLabelAction
implements CockpitAction<ConsignmentModel, ConsignmentModel> {
    private static final Logger LOG = LoggerFactory.getLogger(CustomPrintPackLabelAction.class);
    protected static final String PACK_CONSIGNMENT_CHOICE = "packConsignment";
    protected static final String PACKING_TEMPLATE_CODE = "NPR_Packing";
    protected static final String CAPTURE_PAYMENT_ON_CONSIGNMENT = "warehousing.capturepaymentonconsignment";
    @Resource
    private ConsignmentPrintDocumentStrategy consignmentPrintPackSlipStrategy;
    @Resource
    private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;
    @Resource
    private ConfigurationService configurationService;

    public ActionResult<ConsignmentModel> perform(ActionContext<ConsignmentModel> consignmentModelActionContext) {
        ConsignmentModel consignment = (ConsignmentModel)consignmentModelActionContext.getData();
        LOG.info("Generating Pack Label for consignment {}", (Object)consignment.getCode());
        WorkflowActionModel packWorkflowAction = this.getWarehousingConsignmentWorkflowService().getWorkflowActionForTemplateCode(PACKING_TEMPLATE_CODE, consignment);
        if (packWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals((Object)packWorkflowAction.getStatus())) {
            try {
                this.getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, PACKING_TEMPLATE_CODE, PACK_CONSIGNMENT_CHOICE);
            }
            catch (BusinessProcessException businessProcessException) {
                LOG.info("Unable to trigger pack consignment process for consignment: {}", (Object)consignment.getCode());
            }
        }
        this.getConsignmentPrintPackSlipStrategy().printDocument(consignment);
        return new ActionResult("success");
    }

    public boolean canPerform(ActionContext<ConsignmentModel> consignmentModelActionContext) {
        ConsignmentModel consignment = consignmentModelActionContext.getData();
        return !Objects.isNull(consignment) && !Objects.isNull(consignment.getOrder()) && !ConsignmentStatus.CANCELLED.equals(consignment.getStatus()) && consignment.getFulfillmentSystemConfig() == null && !((OrderModel) consignment.getOrder()).isSyncPartialCancelFailed();
    }

    public boolean needsConfirmation(ActionContext<ConsignmentModel> consignmentModelActionContext) {
        return false;
    }

    public String getConfirmationMessage(ActionContext<ConsignmentModel> consignmentModelActionContext) {
        return null;
    }

    protected ConsignmentPrintDocumentStrategy getConsignmentPrintPackSlipStrategy() {
        return this.consignmentPrintPackSlipStrategy;
    }

    protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService() {
        return this.warehousingConsignmentWorkflowService;
    }

    protected ConfigurationService getConfigurationService() {
        return this.configurationService;
    }
}

