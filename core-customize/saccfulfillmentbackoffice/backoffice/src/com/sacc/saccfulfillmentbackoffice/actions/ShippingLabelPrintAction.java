package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.google.common.base.Preconditions;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.context.FulfillmentProviderContext;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;


/**
 *
 * @author mnasro
 *
 */
public class ShippingLabelPrintAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ShippingLabelPrintAction.class);
	private static final String AIRWAY_BILL_NOT_FOUND = "Air Waybill not found";
	private static final String CARRIER_PROVIDER_NOT_FOUND = "Carrier Fulfillment provider not found";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	protected void checkData(final byte[] obj)
	{
		if (obj == null || obj.length == 0)
		{
			throw new IllegalArgumentException(AIRWAY_BILL_NOT_FOUND);
		}
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();
		try
		{
			final FulfillmentProviderType type = consignment.getCarrierDetails().getFulfillmentProviderType();
			if (type == null)
			{
				Messagebox.show(CARRIER_PROVIDER_NOT_FOUND);
			}

			final Optional<byte[]> printAWB = getFulfillmentContext().printAWB(consignment, type);
			if (printAWB.isPresent())
			{
				Filedownload.save(printAWB.get(), MediaType.APPLICATION_PDF_VALUE, consignment.getTrackingID());
			}
			else
			{
				Messagebox.show(AIRWAY_BILL_NOT_FOUND);
			}
		}
		catch (final com.sacc.saccfulfillment.exception.FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(consignment.getOrder().getStore()),
					consignment.getOrder(), ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage()));
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return consignment != null && consignment.getCarrierDetails() != null && provider.isPresent()
				&& Boolean.TRUE.equals(provider.get().getActive());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return "Do you want to print AWB?";
	}

	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SMSAFULFILLMENTPROVIDER":
				return IntegrationProvider.SMSA;
			case "SAEEFULFILLMENTPROVIDER":
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
