/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpaymentfacades.constants;

/**
 * Global class for all Saccpaymentfacades constants. You can add global constants for your extension into this class.
 */
public final class SaccpaymentfacadesConstants extends GeneratedSaccpaymentfacadesConstants
{
	public static final String EXTENSIONNAME = "saccpaymentfacades";

	private SaccpaymentfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccpaymentfacadesPlatformLogo";
}
