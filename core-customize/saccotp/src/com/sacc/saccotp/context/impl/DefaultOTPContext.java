/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.sacc.saccotp.context.OTPContext;
import com.sacc.saccotp.context.OTPProviderContext;
import com.sacc.saccotp.entity.SessionData;
import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccotp.model.OTPProviderModel;
import com.sacc.saccotp.strategy.OTPStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOTPContext.
 *
 * @author mnasro
 */
public class DefaultOTPContext implements OTPContext
{

	/** The Constant OTP_STRATEGY_NOT_FOUND. */
	private static final String OTP_STRATEGY_NOT_FOUND = "strategy not found";

	/** The otp provider context. */
	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Gets the otp provider context.
	 *
	 * @return the otp provider context
	 */
	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	/** The payment strategy map. */
	@Resource(name = "otpStrategyMap")
	private Map<Class<?>, OTPStrategy> otpStrategyMap;


	protected Map<Class<?>, OTPStrategy> getOtpStrategyMap()
	{
		return otpStrategyMap;
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<OTPStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPStrategy strategy = getOtpStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, OTP_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}


	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param otpProviderModel
	 *           the otp provider model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel) throws OTPException
	{
		getStrategy(otpProviderModel.getClass()).get().sendOTPCode(countryisoCode, mobileNumber, otpProviderModel);
	}

	/**
	 * Send OTP code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param cmsSiteModel
	 *           the cms site model
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCode(final String countryisoCode, final String mobileNumber, final CMSSiteModel cmsSiteModel)
			throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		sendOTPCode(countryisoCode, mobileNumber, OTPProviderModel.get());
	}

	/**
	 * Send OTP code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public void sendOTPCodeByCurrentSite(final String countryisoCode, final String mobileNumber) throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		sendOTPCode(countryisoCode, mobileNumber, OTPProviderModel.get());
	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param otpProviderModel
	 *           the otp provider model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		return getStrategy(otpProviderModel.getClass()).get().verifyCode(countryisoCode, mobileNumber, code, otpProviderModel);

	}

	/**
	 * Verify code.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final CMSSiteModel cmsSiteModel) throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		return verifyCode(countryisoCode, mobileNumber, code, OTPProviderModel.get());
	}

	/**
	 * Verify code by current site.
	 *
	 * @param countryisoCode
	 *           the countryiso code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	@Override
	public boolean verifyCodeByCurrentSite(final String countryisoCode, final String mobileNumber, final String code)
			throws OTPException
	{
		final Optional<OTPProviderModel> OTPProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		Preconditions.checkArgument(OTPProviderModel.isPresent(), "OTPProviderModel is null");
		return verifyCode(countryisoCode, mobileNumber, code, OTPProviderModel.get());
	}

	@Override
	public boolean isEnabled(final CMSSiteModel cmsSiteModel)
	{
		if (cmsSiteModel == null || cmsSiteModel.getRegistrationOTPProvider() == null
				|| StringUtils.isBlank(cmsSiteModel.getRegistrationOTPProvider()))
		{
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel);

		if (otpProviderModel.isPresent())
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean isEnabledByCurrentSite()
	{
		Optional<OTPProviderModel> otpProviderModel = Optional.empty();
		try {
			otpProviderModel = getOtpProviderContext().getProviderByCurrentSite();
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return false;
		}
		if (otpProviderModel.isPresent())
		{
			return true;
		}
		return false;
	}

	@Override
	public void sendOTPCodeByCurrentSiteAndSessionData(final String countryisoCode, final String mobileNumber,
			final SessionData data) throws OTPException
	{
		Preconditions.checkArgument(data != null, "data is null");
		Preconditions.checkArgument(data.getData() != null, "data object is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(data.getSessionKey()), "data SessionKey is null or empty");

		sendOTPCodeByCurrentSite(countryisoCode, mobileNumber);
		getSessionService().setAttribute(data.getSessionKey(), data);
		getSessionService().setAttribute("isSend", Boolean.TRUE);

	}

	@Override
	public Optional<SessionData> getSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), "data SessionKey is null or empty");

		final Object data = getSessionService().getAttribute(sessionKey);
		if (data == null || !(data instanceof SessionData))
		{
			return Optional.empty();
		}
		return Optional.ofNullable((SessionData) data);
	}

	@Override
	public void removeSessionData(final String sessionKey)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionKey), "data SessionKey is null or empty");

		getSessionService().removeAttribute(sessionKey);

	}


}
