/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccotp.service;

import com.sacc.saccotp.exception.OTPException;


/**
 * @author mnasro
 *
 *         The Interface OTPService.
 */
public interface OTPService
{

	/**
	 * Send OTP code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendOTPCode(final String countryCode, final String mobileNumber)
			throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param countryCode
	 *           the country code
	 * @param mobileNumber
	 *           the mobile number
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String countryCode, final String mobileNumber,
			final String code) throws OTPException;


}
