/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.sacc.saccordermanagementcustomwebservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.sacc.saccordermanagementcustomwebservices.constants.SaccordermanagementcustomwebservicesConstants;

@SuppressWarnings("PMD")
public class SaccordermanagementcustomwebservicesManager extends GeneratedSaccordermanagementcustomwebservicesManager
{
	public static final SaccordermanagementcustomwebservicesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SaccordermanagementcustomwebservicesManager) em.getExtension(SaccordermanagementcustomwebservicesConstants.EXTENSIONNAME);
	}
	
}
