/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorder.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.sacc.saccorder.model.OrderNoteEntryModel;
import com.sacc.saccorder.model.OrderNoteModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderNoteService
{
	public List<OrderNoteModel> getOrderNotesByCurrentSite();

	public List<OrderNoteModel> getOrderNotes(final CMSSiteModel cmsSiteModel);

	public void saveOrderNoteEntry(final OrderNoteEntryModel noteModel, final CartModel cart);
}
