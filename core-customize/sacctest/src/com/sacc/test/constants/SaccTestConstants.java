/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.test.constants;

/**
 * 
 */
public class SaccTestConstants extends GeneratedSaccTestConstants
{

	public static final String EXTENSIONNAME = "sacctest";

}
