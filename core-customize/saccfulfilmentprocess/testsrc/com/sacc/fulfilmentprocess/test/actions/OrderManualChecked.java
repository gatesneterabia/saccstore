/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.fulfilmentprocess.test.actions;

/**
 * Test counterpart for {@link com.sacc.fulfilmentprocess.actions.order.OrderManualCheckedAction}
 */
public class OrderManualChecked extends TestActionTemp
{
	//EMPTY
}
