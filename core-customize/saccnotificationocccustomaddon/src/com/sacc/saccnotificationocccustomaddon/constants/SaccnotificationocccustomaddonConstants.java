/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccnotificationocccustomaddon.constants;

/**
 * Global class for all saccnotificationocccustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class SaccnotificationocccustomaddonConstants extends GeneratedSaccnotificationocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "saccnotificationocccustomaddon"; //NOSONAR

	private SaccnotificationocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
