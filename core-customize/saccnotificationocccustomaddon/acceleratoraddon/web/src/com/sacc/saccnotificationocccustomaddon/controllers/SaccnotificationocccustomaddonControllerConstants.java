/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccnotificationocccustomaddon.controllers;

/**
 */
public interface SaccnotificationocccustomaddonControllerConstants
{
	// implement here controller constants used by this extension
	String NOTIFICATION_PREFERENCE_NAME = "notificationPreference";
}
