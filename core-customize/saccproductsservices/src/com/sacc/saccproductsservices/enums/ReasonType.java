/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.enums;

/**
 *
 */
public enum ReasonType
{
	CURRENCY_NOT_FOUND, PRODUCT_NOT_FOUND, PRICE_NOT_CREATED, PRICE_NOT_UPDATED, PRODUCT_NOT_CREATED, PRODUCT_NOT_UPDATED, WAREHOUSE_NOT_FOUND, STOCKLEVEL_NOT_CREATED, STOCKLEVEL_NOT_UPDATED;
}
