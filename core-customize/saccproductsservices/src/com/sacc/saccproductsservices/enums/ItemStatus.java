/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.enums;

/**
 *
 */
public enum ItemStatus
{

	UPDATE, NEW
}
