/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.beans;

import com.sacc.saccproductsservices.enums.ReasonType;


/**
 *
 */
public class FailedRecord
{

	private String RecordCode;
	private String Reason;
	private final ReasonType reasonType;

	/**
	 *
	 */
	public FailedRecord(final String recordCode, final String reason, final ReasonType reasonType)
	{
		super();
		RecordCode = recordCode;
		Reason = reason;
		this.reasonType = reasonType;
	}


	/**
	 * @return the reasonType
	 */
	public ReasonType getReasonType()
	{
		return reasonType;
	}

	/**
	 * @return the recordCode
	 */
	public String getRecordCode()
	{
		return RecordCode;
	}

	/**
	 * @param recordCode
	 *           the recordCode to set
	 */
	public void setRecordCode(final String recordCode)
	{
		RecordCode = recordCode;
	}

	/**
	 * @return the reason
	 */
	public String getReason()
	{
		return Reason;
	}

	/**
	 * @param reason
	 *           the reason to set
	 */
	public void setReason(final String reason)
	{
		Reason = reason;
	}



}
