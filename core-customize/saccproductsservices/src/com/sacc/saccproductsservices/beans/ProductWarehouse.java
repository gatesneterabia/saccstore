/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.beans;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;


/**
 *
 */
public class ProductWarehouse
{
	private final ProductModel product;
	private final WarehouseModel warehouse;

	/**
	 *
	 */
	public ProductWarehouse(final ProductModel product, final WarehouseModel warehouse)
	{
		super();
		this.product = product;
		this.warehouse = warehouse;
	}

	/**
	 * @return the product
	 */
	public ProductModel getProduct()
	{
		return product;
	}

	/**
	 * @return the warehouse
	 */
	public WarehouseModel getWarehouse()
	{
		return warehouse;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final ProductWarehouse other = (ProductWarehouse) obj;
		if (product == null)
		{
			if (other.product != null)
			{
				return false;
			}
		}
		else if (!product.equals(other.product))
		{
			return false;
		}
		if (warehouse == null)
		{
			if (other.warehouse != null)
			{
				return false;
			}
		}
		else if (!warehouse.equals(other.warehouse))
		{
			return false;
		}
		return true;
	}


}
