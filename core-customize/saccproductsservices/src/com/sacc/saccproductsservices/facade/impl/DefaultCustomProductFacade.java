/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.facade.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccproductsservices.beans.FailedRecord;
import com.sacc.saccproductsservices.beans.ProductWarehouse;
import com.sacc.saccproductsservices.beans.Result;
import com.sacc.saccproductsservices.enums.ItemStatus;
import com.sacc.saccproductsservices.enums.ReasonType;
import com.sacc.saccproductsservices.facade.CustomProductFacade;
import com.sacc.saccproductsservices.productsData.ApprovalStatus;
import com.sacc.saccproductsservices.productsData.ProductData;
import com.sacc.saccproductsservices.productsData.ProductPriceData;
import com.sacc.saccproductsservices.productsData.ProductsStockLevelData;


/**
 * @author Husam Dababneh husam.dababneh@erabia.com
 */
public class DefaultCustomProductFacade implements CustomProductFacade
{

	@Resource(name = "modelService")
	ModelService modelService;
	@Resource(name = "importService")
	ImportService importService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "priceService")
	private PriceService priceService;
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	@Resource(name = "warehouseService")
	private WarehouseService warehouseService;
	@Resource(name = "stockService")
	private StockService stockService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "unitService")
	private UnitService unitService;

	@Override
	public Result insertUpdateProduct(final String catalogVersion, final String catalogVersionId, final List<ProductData> data)
	{

		Preconditions.checkArgument(data != null && !data.isEmpty(), "data should not be null");
		Preconditions.checkArgument(catalogVersion != null && !catalogVersion.isBlank(),
				"catalogVersion should not be empty or null");
		Preconditions.checkArgument(catalogVersionId != null && !catalogVersionId.isBlank(),
				"catalogVersionId should not be empty or null");
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(catalogVersionId, catalogVersion);
		catalogVersionService.setSessionCatalogVersion(catalogVersionId, catalogVersion);

		final Result result = new Result();
		final Map<ItemStatus, List<ProductData>> productMapStatus = getProductMapStatus(data);


		final List<ProductData> createProducts = productMapStatus.get(ItemStatus.NEW);
		if (CollectionUtils.isEmpty(createProducts))
		{
			for (final ProductData productData : createProducts)
			{
				try
				{
					final ProductModel productModel = modelService.create(ProductModel.class);
					productModel.setCode(productData.getCode());
					productModel.setCatalogVersion(catalogVersionModel);
					createUpdateProduct(productModel, productData);
					result.getSuccessful().getCreatedRecords().add(productData);
				}
				catch (final Exception e)
				{
					result.getFailed().add(new FailedRecord(productData.getCode(), "Couldn't Create Product : " + e.getMessage(),
							ReasonType.PRODUCT_NOT_CREATED));
				}


			}
		}

		final List<ProductData> updateProducts = productMapStatus.get(ItemStatus.UPDATE);
		if (CollectionUtils.isEmpty(updateProducts))
		{
			for (final ProductData productData : updateProducts)
			{
				try
				{
					final ProductModel productModel = productService.getProductForCode(productData.getCode());
					createUpdateProduct(productModel, productData);
					result.getSuccessful().getUpdatedRecords().add(productData);

				}
				catch (final Exception e)
				{
					result.getFailed().add(new FailedRecord(productData.getCode(), "Couldn't Update Product : " + e.getMessage(),
							ReasonType.PRODUCT_NOT_UPDATED));
				}
			}
		}
		return result;

	}


	/**
	 *
	 */
	private void createUpdateProduct(final ProductModel productModel, final ProductData productData)
	{
		final ApprovalStatus approved = productData.getApproved() != null ? productData.getApproved() : ApprovalStatus.APPROVED;
		final Map<java.util.Locale, String> nameMap = productData.getName();
		final Map<java.util.Locale, String> SummaryMap = productData.getSummary();
		final Map<java.util.Locale, String> descriptioMap = productData.getDescription();
		final double weight = productData.getWeight();
		final List<String> superCategoriesCodes = productData.getSuperCategoriesCodes();


		// @TODO
		if (superCategoriesCodes != null && !superCategoriesCodes.isEmpty())
		{
			final StringBuffer superCategoriesCodesStr = new StringBuffer();
			for (int i = 0; i < superCategoriesCodes.size(); i++)
			{
				superCategoriesCodesStr.append(superCategoriesCodes.get(i));
				if (i < superCategoriesCodes.size() - 1)
				{

					superCategoriesCodesStr.append(",");
				}

			}
		}
		productModel.setWeight(weight);
		if (nameMap != null && !nameMap.isEmpty())
		{
			for (final Locale key : nameMap.keySet())
			{
				productModel.setName(nameMap.get(key), key);
			}
		}

		if (SummaryMap != null && !SummaryMap.isEmpty())
		{
			for (final Locale key : SummaryMap.keySet())
			{
				productModel.setName(SummaryMap.get(key), key);
			}
		}
		if (descriptioMap != null && !descriptioMap.isEmpty())
		{
			for (final Locale key : descriptioMap.keySet())
			{
				productModel.setName(descriptioMap.get(key), key);
			}
		}

		switch (approved)
		{
			case CHECK:
				productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);

				break;
			case APPROVED:
				productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

				break;
			case UNAPPROVED:
				productModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);

				break;
		}

		modelService.saveAll(productModel);
	}

	/**
	 *
	 */
	private Map<ItemStatus, List<ProductData>> getProductMapStatus(final List<ProductData> data)
	{

		final Map<ItemStatus, List<ProductData>> map = new HashMap<>();

		map.put(ItemStatus.NEW, new ArrayList<>());
		map.put(ItemStatus.UPDATE, new ArrayList<>());

		for (final ProductData productData : data)
		{
			try
			{
				final ProductModel productModel = productService.getProductForCode(productData.getCode());
				if (productModel != null)
				{
					map.get(ItemStatus.UPDATE).add(productData);
				}
				else
				{
					map.get(ItemStatus.NEW).add(productData);
				}
			}
			catch (final Exception e)
			{
				map.get(ItemStatus.NEW).add(productData);
			}
		}

		return map;
	}


	@Override
	public Result insertUpdatePrice(final String catalogVersion, final String catalogVersionId, final List<ProductPriceData> data,
			final boolean checkNet, final boolean checkCurrency)
	{
		Preconditions.checkArgument(data != null && !data.isEmpty(), "data should not be null");
		Preconditions.checkArgument(catalogVersion != null && !catalogVersion.isBlank(),
				"catalogVersion should not be empty or null");
		Preconditions.checkArgument(catalogVersionId != null && !catalogVersionId.isBlank(),
				"catalogVersionId should not be empty or null");

		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(catalogVersionId, catalogVersion);
		catalogVersionService.setSessionCatalogVersion(catalogVersionId, catalogVersion);

		final Result result = new Result();

		for (final ProductPriceData productPriceData : data)
		{
			CurrencyModel currencyModel = null;
			ProductModel productModel = null;

			boolean failed = false;
			try
			{
				currencyModel = commonI18NService.getCurrency(productPriceData.getCurrencyIsoCode());
				if (currencyModel == null)
				{

					result.getFailed().add(
							new FailedRecord(productPriceData.getProductCode(), "Currency Not found", ReasonType.CURRENCY_NOT_FOUND));
					failed = true;
				}
			}
			catch (final Exception e)
			{
				result.getFailed().add(new FailedRecord(productPriceData.getProductCode(), "Currency Not found: " + e.getMessage(),
						ReasonType.CURRENCY_NOT_FOUND));
				failed = true;
			}
			try
			{
				productModel = productService.getProductForCode(productPriceData.getProductCode());
				if (productModel == null)
				{

					result.getFailed().add(
							new FailedRecord(productPriceData.getProductCode(), "Product  Not found", ReasonType.PRODUCT_NOT_FOUND));
					failed = true;
				}
			}
			catch (final Exception e)
			{
				result.getFailed().add(new FailedRecord(productPriceData.getProductCode(), "Product  Not found: " + e.getMessage(),
						ReasonType.PRODUCT_NOT_FOUND));
				failed = true;
			}
			if (failed)
			{
				continue;
			}
			final Collection<PriceRowModel> europe1Prices = productModel.getEurope1Prices();
			if (CollectionUtils.isEmpty(europe1Prices))
			{
				try
				{
					createPriceRow(productModel, catalogVersionModel, currencyModel, productPriceData);
					result.getSuccessful().getCreatedRecords().add(productPriceData);

				}
				catch (final Exception e)
				{
					result.getFailed().add(new FailedRecord(productPriceData.getProductCode(),
							"Couldn't Create Price Row: " + e.getMessage(), ReasonType.PRICE_NOT_CREATED));
				}

			}
			else
			{

				final CurrencyModel tempCurrencyModel = currencyModel;
				final List<PriceRowModel> collect = europe1Prices.stream().filter(Objects::nonNull)
						.filter(row -> (checkCurrency ? tempCurrencyModel.equals(row.getCurrency()) : true)
								&& (checkNet ? Boolean.valueOf(productPriceData.isNet()).equals(row.getNet()) : true))
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(collect))
				{
					try
					{
						createPriceRow(productModel, catalogVersionModel, currencyModel, productPriceData);
						result.getSuccessful().getCreatedRecords().add(productPriceData);

					}
					catch (final Exception e)
					{
						result.getFailed().add(new FailedRecord(productPriceData.getProductCode(),
								"Couldn't Create Price Row: " + e.getMessage(), ReasonType.PRICE_NOT_CREATED));
					}
				}
				else if (!CollectionUtils.isEmpty(collect))
				{
					collect.forEach(row -> {
						try
						{
							updatePriceRow(row, productPriceData, catalogVersionModel, checkNet, checkCurrency);
							result.getSuccessful().getUpdatedRecords().add(productPriceData);

						}
						catch (final Exception e)
						{
							result.getFailed().add(new FailedRecord(productPriceData.getProductCode(),
									"Couldn't Updated Price Row: " + e.getMessage(), ReasonType.PRICE_NOT_UPDATED));
						}
					});
				}
			}

		}

		return result;
	}


	/**
	 *
	 */
	private void updatePriceRow(final PriceRowModel row, final ProductPriceData productPriceData,
			final CatalogVersionModel catalogVersionModel, final boolean checkNet, final boolean checkCurrency)
	{
		if (!checkNet)
		{
			row.setNet(checkNet);
		}
		if (!checkCurrency)
		{
			row.setNet(checkCurrency);
		}
		row.setCatalogVersion(catalogVersionModel);
		row.setPrice(productPriceData.getPrice());
		row.setOriginalPrice(productPriceData.getOriginalPrice());
		modelService.save(row);

	}

	private PriceRowModel createPriceRow(final ProductModel product, final CatalogVersionModel catalogVersionModel,
			final CurrencyModel currencyModel, final ProductPriceData productPriceData)
	{
		final PriceRowModel priceRow = modelService.create(PriceRowModel.class);
		priceRow.setCurrency(currencyModel);
		priceRow.setCatalogVersion(catalogVersionModel);
		priceRow.setPrice(productPriceData.getPrice());
		priceRow.setOriginalPrice(productPriceData.getOriginalPrice());
		priceRow.setProduct(product);
		priceRow.setNet(productPriceData.isNet());
		modelService.save(priceRow);
		modelService.refresh(priceRow);
		return priceRow;
	}

	@Override
	public Result insertUpdateStockLevel(final String catalogVersion, final String catalogVersionId,
			final List<ProductsStockLevelData> data)
	{
		Preconditions.checkArgument(data != null && !data.isEmpty(), "data should not be null");
		Preconditions.checkArgument(catalogVersion != null && !catalogVersion.isBlank(),
				"catalogVersion should not be empty or null");
		Preconditions.checkArgument(catalogVersionId != null && !catalogVersionId.isBlank(),
				"catalogVersionId should not be empty or null");


		final Result result = new Result();
		final CatalogVersionModel sourceVersion = catalogVersionService.getCatalogVersion(catalogVersionId, catalogVersion);
		catalogVersionService.setSessionCatalogVersion(catalogVersionId, catalogVersion);

		final Map<ProductWarehouse, List<ProductsStockLevelData>> productWarehouseMap = new HashMap<>();
		for (final ProductsStockLevelData productsStockLevelData : data)
		{
			ProductWarehouse productWarehouse = null;

			ProductModel productModel = null;
			WarehouseModel warehouseModel = null;
			boolean failed = false;
			try
			{
				productModel = productService.getProductForCode(productsStockLevelData.getProductCode());
			}
			catch (final Exception e)
			{
				failed = true;
				result.getFailed().add(new FailedRecord(productsStockLevelData.getProductCode(),
						"Product not Found!: " + e.getMessage(), ReasonType.PRODUCT_NOT_FOUND));
			}
			try
			{
				warehouseModel = warehouseService.getWarehouseForCode(productsStockLevelData.getWarehouseCode());
			}
			catch (final Exception e)
			{
				failed = true;
				result.getFailed().add(new FailedRecord(productsStockLevelData.getProductCode(),
						"Warehouse not Found!: " + e.getMessage(), ReasonType.WAREHOUSE_NOT_FOUND));
			}

			if (failed && (productModel == null || warehouseModel == null))
			{
				continue;

			}
			productWarehouse = new ProductWarehouse(productModel, warehouseModel);
			if (!productWarehouseMap.containsKey(productWarehouse))
			{
				productWarehouseMap.put(productWarehouse, new ArrayList<ProductsStockLevelData>());
			}

			productWarehouseMap.get(productWarehouse).add(productsStockLevelData);

		}

		if (productWarehouseMap == null || productWarehouseMap.isEmpty())
		{
			return result;
		}
		for (final ProductWarehouse productWarehouse : productWarehouseMap.keySet())
		{
			final List<ProductsStockLevelData> list = productWarehouseMap.get(productWarehouse);
			StockLevelModel stockLevel = null;
			try
			{
				stockLevel = stockService.getStockLevel(productWarehouse.getProduct(), productWarehouse.getWarehouse());
			}
			catch (final Exception e)
			{
				for (final ProductsStockLevelData productsStockLevelData : list)
				{
					try
					{
						createStockLevel(productWarehouse.getProduct(), productWarehouse.getWarehouse(), productsStockLevelData);
						result.getSuccessful().getCreatedRecords().add(productsStockLevelData);

					}
					catch (final Exception ex)
					{
						result.getFailed().add(new FailedRecord(productsStockLevelData.getProductCode(),
								"Couldn't Create StockLevel : " + e.getMessage(), ReasonType.STOCKLEVEL_NOT_CREATED));
					}
				}
			}
			if (stockLevel != null)
			{
				for (final ProductsStockLevelData productsStockLevelData : list)
				{
					try
					{
						updateStockLevel(productWarehouse.getProduct(), stockLevel, productsStockLevelData);
						result.getSuccessful().getUpdatedRecords().add(productsStockLevelData);
					}
					catch (final Exception ex)
					{
						result.getFailed().add(new FailedRecord(productsStockLevelData.getProductCode(),
								"Couldn't Create StockLevel : " + ex.getMessage(), ReasonType.STOCKLEVEL_NOT_CREATED));
					}

				}
			}
			else
			{
				for (final ProductsStockLevelData productsStockLevelData : list)
				{
					try
					{
						createStockLevel(productWarehouse.getProduct(), productWarehouse.getWarehouse(), productsStockLevelData);
						result.getSuccessful().getCreatedRecords().add(productsStockLevelData);
					}
					catch (final Exception ex)
					{
						result.getFailed().add(new FailedRecord(productsStockLevelData.getProductCode(),
								"Couldn't Create StockLevel : " + ex.getMessage(), ReasonType.STOCKLEVEL_NOT_CREATED));
					}
				}
			}


		}
		return result;
	}



	private void createStockLevel(final ProductModel product, final WarehouseModel warehouse,
			final ProductsStockLevelData productsStockLevelData)
	{
		final StockLevelModel newStock = modelService.create(StockLevelModel.class);
		if (productsStockLevelData.getInStockStatus() != null)
		{
			switch (productsStockLevelData.getInStockStatus())
			{
				case FORCEINSTOCK:
					newStock.setInStockStatus(InStockStatus.FORCEINSTOCK);
					break;
				case FORCEOUTOFSTOCK:
					newStock.setInStockStatus(InStockStatus.FORCEOUTOFSTOCK);
					break;
				case NOTSPECIFIED:
					newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
					break;

			}
		}
		newStock.setWarehouse(warehouse);
		newStock.setAvailable(productsStockLevelData.getAvailable());
		newStock.setProduct(product);
		newStock.setProductCode(product.getCode());
		modelService.save(newStock);
	}

	private void updateStockLevel(final ProductModel product, final StockLevelModel stockLevel,
			final ProductsStockLevelData productsStockLevelData)
	{
		stockLevel.setReserved(productsStockLevelData.getReserved());
		stockLevel.setAvailable(productsStockLevelData.getAvailable());
		if (productsStockLevelData.getInStockStatus() != null)
		{
			switch (productsStockLevelData.getInStockStatus())
			{
				case FORCEINSTOCK:
					stockLevel.setInStockStatus(InStockStatus.FORCEINSTOCK);
					break;
				case FORCEOUTOFSTOCK:
					stockLevel.setInStockStatus(InStockStatus.FORCEOUTOFSTOCK);
					break;
				case NOTSPECIFIED:
					stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
					break;

			}
		}
		stockLevel.setInventoryEvents(null);
		modelService.save(stockLevel);
	}

}
