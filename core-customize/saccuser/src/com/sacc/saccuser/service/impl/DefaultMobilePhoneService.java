package com.sacc.saccuser.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccuser.service.MobilePhoneService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


/**
 * The Class DefaultMobilePhoneService.
 *
 * @author mnasro
 */
public class DefaultMobilePhoneService implements MobilePhoneService
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * Validate and normalize phone number by iso code.
	 *
	 * @param countryIsoCode
	 *           the country iso code
	 * @param number
	 *           the number
	 * @return the Optional<String>
	 */
	@Override
	public Optional<String> validateAndNormalizePhoneNumberByIsoCode(final String countryIsoCode, final String number)
	{
		validateParameterNotNull(countryIsoCode, "countryIsoCode cannot be null");
		validateParameterNotNull(countryIsoCode, "countryIsoCode cannot be null");

		final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber = null;
		try
		{
			phoneNumber = phoneNumberUtil.parse(number, countryIsoCode);
		}
		catch (final NumberParseException e)
		{
			return Optional.empty();
		}

		final boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);

		if (!isValid)
		{
			return Optional.empty();
		}
		final String e164FormatNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
		if (e164FormatNumber == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(e164FormatNumber.replaceAll("\\+", ""));
	}

}
