/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccaptchacustomaddon.constants;

/**
 * Global class for all Captchaaddon constants. You can add global constants for your extension into this class.
 */
public final class CaptchaaddonConstants extends GeneratedCaptchaaddonConstants
{
	public static final String EXTENSIONNAME = "sacccaptchacustomaddon";

	private CaptchaaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
