/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccaptchacustomaddon.controllers;

/**
 */
public interface CaptchaaddonControllerConstants
{
	// implement here controller constants used by this extension
}
