/*
 *
 */
package com.sacc.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.dao.CreditCardPaymentSubscriptionDao;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.strategies.CreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.sacc.facades.facade.CustomAcceleratorCheckoutFacade;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.context.PaymentContext;
import com.sacc.saccpayment.context.PaymentProviderContext;
import com.sacc.saccpayment.customer.service.CustomCustomerAccountService;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentModeType;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccstorecredit.service.StoreCreditModeService;
import com.sacc.saccstorecredit.service.StoreCreditService;
import com.sacc.saccstorecreditfacades.data.StoreCreditModeData;
import com.sacc.saccstorecreditfacades.facade.StoreCreditFacade;
import com.sacc.saccstorecreditfacades.facade.StoreCreditModeFacade;
import com.sacc.sacctimeslot.model.TimeSlotInfoModel;
import com.sacc.sacctimeslot.service.TimeSlotService;
import com.sacc.sacctimeslotfacades.TimeSlotInfoData;


/**
 * The Class DefaultCustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 *
 */
public class DefaultCustomAcceleratorCheckoutFacade extends DefaultAcceleratorCheckoutFacade
		implements CustomAcceleratorCheckoutFacade
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomAcceleratorCheckoutFacade.class);

	/** The Constant SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";

	/** The Constant SIGNATURE_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";

	/** The Constant PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";

	/** The Constant ORDER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";

	/** The Constant CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";

	/** The Constant AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG. */
	private static final String AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG = "AuthReplyData cannot be null";

	/** The Constant DECISION_CANNOT_BE_NULL_MSG. */
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";

	/** The Constant CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG. */
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";
	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	/** The customer account service. */
	@Resource(name = "customerAccountService")
	private CustomCustomerAccountService customerAccountService;

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The payment context. */
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private PaymentTransactionStrategy paymentTransactionStrategy;

	/** The credit card payment info create strategy. */
	@Resource(name = "creditCardPaymentInfoCreateStrategy")
	private CreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy;

	/** The credit card payment subscription dao. */
	@Resource(name = "creditCardPaymentSubscriptionDao")
	private CreditCardPaymentSubscriptionDao creditCardPaymentSubscriptionDao;

	/** The payment subscription result data converter. */
	@Resource(name = "paymentSubscriptionResultDataConverter")
	private Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> paymentSubscriptionResultDataConverter;


	/** The payment mode service. */
	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;

	@Resource(name = "timeSlotInfoReverseConverter")
	private Converter<TimeSlotInfoData, TimeSlotInfoModel> timeSlotInfoReverseConverter;

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "storeCreditModeFacade")
	private StoreCreditModeFacade storeCreditModeFacade;

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	//	@Resource(name = "orderNoteFacade")
	//	private OrderNoteFacade orderNoteFacade;
	//
	//	@Resource(name = "orderNoteReverseConverter")
	//	private Converter<OrderNoteData, OrderNoteEntryModel> orderNoteReverseConverter;
	//
	//	@Resource(name = "orderNoteService")
	//	private OrderNoteService orderNoteService;

	/**
	 * Gets the payment mode service.
	 *
	 * @return the payment mode service
	 */
	protected PaymentModeService getPaymentModeService()
	{
		return paymentModeService;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModes()
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			return Optional.empty();
		}
		if (cartModel.getDeliveryMode() == null)
		{
			LOG.error("No DeliveryMode select For current cart code : " + cartModel.getDeliveryMode().getCode());
			return Optional.empty();
		}

		if (CollectionUtils.isEmpty(cartModel.getDeliveryMode().getSupportedPaymentModes()))
		{
			LOG.error("No Payment Modes Defined For delivery Mode Code : " + cartModel.getDeliveryMode().getCode());
			return Optional.empty();
		}
		if (cartModel.getTotalPrice() == 0)
		{
			final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
					.filter(Objects::nonNull)
					.filter(p -> Boolean.TRUE.equals(p.getActive()) && PaymentModeType.CONTINUE.equals(p.getPaymentModeType()))
					.collect(Collectors.toList());

			return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
		}
		final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
				.filter(Objects::nonNull)
				.filter(p -> Boolean.TRUE.equals(p.getActive()) && !PaymentModeType.CONTINUE.equals(p.getPaymentModeType()))
				.collect(Collectors.toList());

		return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
	}

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the optional
	 */
	@Override
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddressData", billingAddressData);
		if (checkIfCurrentUserIsTheCartUser())
		{
			final BillingInfo billingInfo = new BillingInfo();
			billingInfo.setCity(billingAddressData.getTown());
			billingInfo.setCountry(billingAddressData.getCountry() == null ? null : billingAddressData.getCountry().getIsocode());
			billingInfo.setRegion(billingAddressData.getRegion() == null ? null : billingAddressData.getRegion().getIsocode());
			billingInfo.setFirstName(billingAddressData.getFirstName());
			billingInfo.setLastName(billingAddressData.getLastName());
			billingInfo.setEmail(billingAddressData.getEmail());
			billingInfo.setPhoneNumber(billingAddressData.getPhone());
			billingInfo.setPostalCode(billingAddressData.getPostalCode());
			billingInfo.setStreet1(billingAddressData.getLine1());
			billingInfo.setStreet2(billingAddressData.getLine2());

			final Optional<NoCardPaymentInfoModel> noCardPaymentInfo = getCustomerAccountService().createPaymentSubscription(
					getCurrentUserForCheckout(), billingInfo, billingAddressData.getTitleCode(), getPaymentProvider(),
					paymentInfoData.isSaved(),
					paymentInfoData.getNoCardTypeData() == null ? null : paymentInfoData.getNoCardTypeData().getCode());

			return noCardPaymentInfo.isPresent()
					? Optional.ofNullable(getNoCardPaymentInfoConverter().convert(noCardPaymentInfo.get()))
					: Optional.empty();
		}
		return Optional.empty();
	}

	/**
	 * Sets the general payment details.
	 *
	 * @param paymentInfoId
	 *           the payment info id
	 * @return true, if successful
	 */
	@Override
	public boolean setGeneralPaymentDetails(final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser() && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = getCurrentUserForCheckout();
			final Optional<PaymentInfoModel> paymentInfo = getCustomerAccountService().getPaymentInfoForCode(currentUserForCheckout,
					paymentInfoId);

			final CartModel cartModel = getCart();
			if (paymentInfo.isPresent())
			{
				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setPaymentInfo(paymentInfo.get());
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			LOG.warn(String.format(
					"Did not find paymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo Will not get set.",
					currentUserForCheckout, cartModel, paymentInfoId));
		}
		return false;
	}

	@Override
	protected void beforePlaceOrder(final CartModel cartModel)
	{
		super.beforePlaceOrder(cartModel);
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		redeemStoreCreditAmount();
		super.afterPlaceOrder(cartModel, orderModel);
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}



	/**
	 * Gets the customer account service.
	 *
	 * @return the customer account service
	 */
	@Override
	protected CustomCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}


	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	@Override
	public void setPaymentMode(final String paymentMode)
	{
		if (getCart() != null)
		{
			validateParameterNotNullStandardMessage("paymentMode", paymentMode);
			final PaymentModeModel paymentModeModel = getPaymentModeService().getPaymentModeForCode(paymentMode);
			getCart().setPaymentMode(paymentModeModel);
			if (PaymentModeType.CARD.equals(paymentModeModel.getPaymentModeType())
					|| PaymentModeType.CONTINUE.equals(paymentModeModel.getPaymentModeType()))
			{
				getCart().setPaymentStatus(PaymentStatus.PAID);
			}
			else
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
			}
			getCart().setCalculated(Boolean.FALSE);

			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}
	}

	/**
	 * Gets the supported payment data.
	 *
	 * @return the supported payment data
	 */
	@Override
	public Optional<PaymentRequestData> getSupportedPaymentData()
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null || cart.getPaymentMode().getPaymentProvider() == null)
		{
			return Optional.empty();
		}
		return getPaymentContext().getPaymentData(cart.getPaymentMode().getPaymentProvider(), cart);
	}

	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	@Override
	public void saveBillingAddress(final AddressData addressData)
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			if (addressData.getId() != null && addressData.getId().equals(getDeliveryAddress().getId()))
			{
				final AddressModel deliveryAddress = sessionCart.getDeliveryAddress();
				deliveryAddress.setBillingAddress(true);
				sessionCart.setPaymentAddress(deliveryAddress);
				getModelService().save(deliveryAddress);
			}
			else
			{
				final AddressModel convertReverse = addressReverseConverter.convert(addressData);
				convertReverse.setOwner(sessionCart.getUser());
				sessionCart.setPaymentAddress(convertReverse);
				getModelService().save(convertReverse);
				getModelService().save(sessionCart);
			}


		}
	}

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	@Override
	public Optional<PaymentProviderModel> getSupportedPaymentProvider()
	{
		final CartModel sessionCart = getCart();
		return sessionCart.getPaymentMode() != null ? Optional.ofNullable(sessionCart.getPaymentMode().getPaymentProvider())
				: Optional.empty();
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null || cart.getPaymentMode().getPaymentProvider() == null)
		{
			return false;
		}
		return getPaymentContext().isSuccessfulPaidOrder(cart.getPaymentMode().getPaymentProvider(), cart, data);
	}

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	@Override
	public Optional<PaymentResponseData> getOrderPaymentResponseData(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null || cart.getPaymentMode().getPaymentProvider() == null)
		{
			return Optional.empty();
		}
		return getPaymentContext().getResponseData(cart.getPaymentMode().getPaymentProvider(), cart, data);
	}

	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	@Override
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null || cart.getPaymentMode().getPaymentProvider() == null)
		{
			return Optional.empty();
		}
		final Optional<CreateSubscriptionResult> response = getPaymentContext().interpretResponse(orderInfoMap,
				cart.getPaymentMode().getPaymentProvider());
		ServicesUtil.validateParameterNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		if (response.isPresent())
		{
			paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()));
			paymentSubscriptionResult.setDecision(String.valueOf(response.get().getDecision()));
			paymentSubscriptionResult.setResultCode(String.valueOf(response.get().getReasonCode()));
		}


		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()))
		{
			final CustomerModel customerModel = (CustomerModel) cart.getUser();
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(customerModel, response.get().getRequestId(), response.get().getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.get().getCustomerInfoData(), response.get().getSubscriptionInfoData(),
					response.get().getPaymentInfoData(), saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null)
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				savePaymentTransactionEntry.setPaymentTransaction(paymentTransaction);
				cart.setPaymentTransactions(Arrays.asList(paymentTransaction));
				getModelService().save(cart);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.get().getDecision(), response.get().getReasonCode());
			LOG.error(logData);
		}


		if (paymentSubscriptionResult != null)
		{
			return Optional.ofNullable(getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResult));
		}
		else
		{
			return Optional.empty();
		}

	}

	@Override
	public void setTimeSlot(final TimeSlotInfoData timeSlotInfoData)
	{
		validateParameterNotNullStandardMessage("timeSlotInfoData", timeSlotInfoData);
		final TimeSlotInfoModel infoModel = timeSlotInfoReverseConverter.convert(timeSlotInfoData);
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			timeSlotService.saveTimeSlotInfo(infoModel, sessionCart, timeSlotInfoData.getDate(), timeSlotInfoData.getStart());
		}
	}

	//	@Override
	//	public void setOrderNote(final OrderNoteData orderNote)
	//	{
	//		validateParameterNotNullStandardMessage("orderNote", orderNote);
	//		final OrderNoteEntryModel noteModel = orderNoteReverseConverter.convert(orderNote);
	//		if (getCart() != null)
	//		{
	//			final CartModel sessionCart = getCart();
	//			orderNoteService.saveOrderNoteEntry(noteModel, sessionCart);
	//		}
	//	}

	/**
	 * Gets the payment subscription result data converter.
	 *
	 * @return the payment subscription result data converter
	 */
	protected Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> getPaymentSubscriptionResultDataConverter()
	{
		return paymentSubscriptionResultDataConverter;
	}

	/**
	 * Gets the credit card payment subscription dao.
	 *
	 * @return the credit card payment subscription dao
	 */
	protected CreditCardPaymentSubscriptionDao getCreditCardPaymentSubscriptionDao()
	{
		return creditCardPaymentSubscriptionDao;
	}

	/**
	 * Gets the credit card payment info create strategy.
	 *
	 * @return the credit card payment info create strategy
	 */
	protected CreditCardPaymentInfoCreateStrategy getCreditCardPaymentInfoCreateStrategy()
	{
		return creditCardPaymentInfoCreateStrategy;
	}

	/**
	 * Gets the payment transaction strategy.
	 *
	 * @return the payment transaction strategy
	 */
	protected PaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	/**
	 * Gets the payment provider context.
	 *
	 * @return the payment provider context
	 */
	protected PaymentProviderContext getPaymentProviderContext()
	{
		return paymentProviderContext;
	}

	/**
	 * Gets the payment context.
	 *
	 * @return the payment context
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes()
	{
		return storeCreditModeFacade.getSupportedStoreCreditModesCurrentBaseStore();
	}

	@Override
	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected)
	{
		if (getCart() != null)
		{
			getCart().setStoreCreditMode(storeCreditModeService.getStoreCreditMode(StoreCreditTypeCode));
			getCart().setStoreCreditAmountSelected(storeCreditAmaountSelected);
			getCart().setCalculated(Boolean.FALSE);
			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.facades.facade.CustomAcceleratorCheckoutFacade#redeemStoreCreditAmount(de.hybris.platform.core.model.
	 * order.AbstractOrderModel)
	 */
	@Override
	public void redeemStoreCreditAmount()
	{
		if (getCart() != null && getCart().getStoreCreditAmount() != null && getCart().getStoreCreditAmount().doubleValue() > 0)
		{
			storeCreditService.redeemStoreCreditAmount(getCart());
		}
	}


	@Override
	public boolean isStoreCreditModeSupported(final String StoreCreditTypeCode)
	{
		return storeCreditModeFacade.isStoreCreditModeSupportedByCurrentBaseStore(StoreCreditTypeCode);
	}

	@Override
	public Optional<PriceData> getAvailableBalanceStoreCreditAmount()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
	}


	@Override
	public Optional<PriceData> getStoreCreditAmountFullRedeem()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountFullRedeem(getCart());
	}

	@Override
	public Optional<PaymentResponseData> getPaymentTransactionStatusResponseData(final Object data) throws PaymentException
	{
		return getPaymentContext().getPaymentOrderStatusResponseDataByCurrentStore(data, getCart());
	}

	@Override
	public boolean isSuccessfulPaidOrderByOrder()
	{
		if (getCart() == null)
		{
			return false;
		}

		return paymentContext.isSuccessfulPaidOrderByOrderTransactions(getCart());
	}

	//	@Override
	//	public List<OrderNoteData> getSupportedOrderNotes()
	//	{
	//		return orderNoteFacade.getOrderNotesByCurrentSite();
	//	}


}

