/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.facades.facade;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccstorecreditfacades.data.StoreCreditModeData;
import com.sacc.sacctimeslotfacades.TimeSlotInfoData;


/**
 * The Interface SefamAcceleratorCheckoutFacade.
 *
 * @author mnasro
 */
public interface CustomAcceleratorCheckoutFacade extends AcceleratorCheckoutFacade
{

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	public Optional<List<PaymentModeData>> getSupportedPaymentModes();


	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	public void setPaymentMode(final String paymentMode);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Optional<PaymentRequestData> getSupportedPaymentData();

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	public Optional<PaymentResponseData> getOrderPaymentResponseData(Object data);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Optional<PaymentProviderModel> getSupportedPaymentProvider();

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(Object data);

	public boolean isSuccessfulPaidOrderByOrder();

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the no card payment info data
	 */
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData);

	/**
	 * Set Payment Details on the cart.
	 *
	 * @param paymentInfoId
	 *           the ID of the payment info to set as the default payment
	 * @return true if operation succeeded
	 */
	boolean setGeneralPaymentDetails(String paymentInfoId);


	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	void saveBillingAddress(AddressData addressData);


	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount);


	void setTimeSlot(TimeSlotInfoData timeSlotInfoData);


	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes();

	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected);

	public void redeemStoreCreditAmount();

	public boolean isStoreCreditModeSupported(final String storeCreditTypeCode);


	public Optional<PriceData> getStoreCreditAmountFullRedeem();

	public Optional<PriceData> getAvailableBalanceStoreCreditAmount();

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 * @throws PaymentException
	 */
	public Optional<PaymentResponseData> getPaymentTransactionStatusResponseData(Object data) throws PaymentException;
}
