package com.sacc.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SaeeFulfillmentProviderModel;
import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;
import com.sacc.saccfulfillment.service.FulfillmentProviderService;



/**
 * @author abu-muhasien
 */
public class GenarateOrderTrackingIdEmailContext extends AbstractEmailContext<ConsignmentProcessModel>
{
	private static final String ORDER_CODE = "orderCode";
	private static final String TRACKING_ID = "trackingId";
	private static final String TRACKING_URL = "trackingURL";
	private static final String SHIPPER_NAME = "shipperName";
	private String orderCode;
	private String trackingId;
	private String shipperName;
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Inits the.
	 *
	 * @param genarateOrderTrackingIdEmailModel
	 *           the email failed notification process model
	 * @param emailPageModel
	 *           the email page model
	 */
	@Override
	public void init(final ConsignmentProcessModel genarateOrderTrackingIdEmailModel, final EmailPageModel emailPageModel)
	{
		super.init(genarateOrderTrackingIdEmailModel, emailPageModel);
		final ConsignmentModel consignment = genarateOrderTrackingIdEmailModel.getConsignment();
		modelService.refresh(consignment);
		if (consignment != null && consignment.getOrder() != null)
		{
			orderCode = consignment.getOrder().getCode();
			trackingId = consignment.getTrackingID();
		}

		put(EMAIL, getCustomerEmailResolutionService().getEmailForCustomer(getCustomer(genarateOrderTrackingIdEmailModel)));
		put(DISPLAY_NAME, getCustomer(genarateOrderTrackingIdEmailModel).getDisplayName());
		put(ORDER_CODE, orderCode);
		put(TRACKING_ID, trackingId);
		if (consignment.getCarrierDetails() != null)
		{
			shipperName = consignment.getCarrierDetails().getFulfillmentProviderType().getCode();
			put(SHIPPER_NAME, shipperName);
		}
		if (consignment.getCarrierDetails() != null
				&& FulfillmentProviderType.SMSA.equals(consignment.getCarrierDetails().getFulfillmentProviderType()))
		{
			final Optional<FulfillmentProviderModel> optional = fulfillmentProviderService
					.get(consignment.getCarrierDetails().getCode(), SmsaFulfillmentProviderModel.class);
			if (optional.isPresent())
			{
				put(TRACKING_URL, optional.get().getTrackingUrl());
			}
		}
		else if (consignment.getCarrierDetails() != null
				&& FulfillmentProviderType.SAEE.equals(consignment.getCarrierDetails().getFulfillmentProviderType()))
		{
			final Optional<FulfillmentProviderModel> optional = fulfillmentProviderService
					.get(consignment.getCarrierDetails().getCode(), SaeeFulfillmentProviderModel.class);
			if (optional.isPresent())
			{
				put(TRACKING_URL, optional.get().getTrackingUrl());
			}
		}
	}

	/**
	 * Gets the site.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final ConsignmentProcessModel consignmentProcessModel)
	{
		return consignmentProcessModel.getConsignment().getOrder().getSite();
	}

	/**
	 * Gets the customer.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final ConsignmentProcessModel consignmentProcessModel)
	{
		return (CustomerModel) consignmentProcessModel.getConsignment().getOrder().getUser();
	}

	/**
	 * Gets the email language.
	 *
	 * @param consignmentProcessModel
	 *           the consignment process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final ConsignmentProcessModel consignmentProcessModel)
	{
		// may throw ClassCastException
		return ((OrderModel) consignmentProcessModel.getConsignment().getOrder()).getLanguage();
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the trackingId
	 */
	public String getTrackingId()
	{
		return trackingId;
	}

	/**
	 * @param trackingId
	 *           the trackingId to set
	 */
	public void setTrackingId(final String trackingId)
	{
		this.trackingId = trackingId;
	}

	/**
	 * @return the shipperName
	 */
	public String getShipperName()
	{
		return shipperName;
	}

	/**
	 * @param shipperName
	 *           the shipperName to set
	 */
	public void setShipperName(final String shipperName)
	{
		this.shipperName = shipperName;
	}


}
