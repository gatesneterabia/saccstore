/**
 *
 */
package com.sacc.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehousing.labels.context.PackContext;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sacc.core.service.BarcodeGenaratorService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomPackContext extends PackContext
{
	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	private OrderData orderData;
	private static final Logger LOG = Logger.getLogger(CustomPackContext.class);

	private MediaModel barcode;

	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());

		if (getConsignment().getBarcode() == null)
		{
			try
			{
				barcode = barcodeGenaratorService.generateBarcodeAsMedia(businessProcessModel.getConsignment());
			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}
		else
		{
			barcode = getConsignment().getBarcode();
		}
	}

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}


	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *           the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}


}
