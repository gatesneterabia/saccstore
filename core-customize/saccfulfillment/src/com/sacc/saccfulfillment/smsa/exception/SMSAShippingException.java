package com.sacc.saccfulfillment.smsa.exception;

import com.sacc.saccfulfillment.smsa.exception.enums.SMSAShippingExceptionType;


/**
 * @author Husam Dababneh
 */
public class SMSAShippingException extends RuntimeException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private final SMSAShippingExceptionType type;


	public SMSAShippingException(final String message, final SMSAShippingExceptionType type)
	{
		super(message);
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public SMSAShippingExceptionType getType()
	{
		return type;
	}
}
