package com.sacc.saccfulfillment.smsa.service.impl;

import java.rmi.RemoteException;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.sacc.saccfulfillment.smsa.config.SMSAShippingConfig;
import com.sacc.saccfulfillment.smsa.config.impl.DefaultSMSAShippingConfig;
import com.sacc.saccfulfillment.smsa.exception.SMSAShippingException;
import com.sacc.saccfulfillment.smsa.exception.enums.SMSAShippingExceptionType;
import com.sacc.saccfulfillment.smsa.model.SMSAShipmentData;
import com.sacc.saccfulfillment.smsa.service.SmsaService;




/**
 * @author Husam Dababneh
 */
public class DefaultSmsaService implements SmsaService
{
	private static final SMSAShippingConfig smsaShippingConfig = new DefaultSMSAShippingConfig();
	private final Logger LOG = Logger.getLogger(DefaultSMSAShippingConfig.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.smsashipping.service.SMSAShippingService#getStatus(java.lang. String,
	 * com.erabia.erabiacarrierbackoffice.model.Object)
	 */
	@Override
	public Optional<String> getStatus(final String awbNumber, final String passkey)
	{
		if (awbNumber == null)
		{
			throw new SMSAShippingException("awb is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}
		if (passkey == null)
		{
			throw new SMSAShippingException("passkey is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}
		String result = null;
		try
		{
			result = smsaShippingConfig.getProxyService().getStatus(awbNumber, passkey);
		}
		catch (final RemoteException e)
		{
			LOG.error("Exception occured while trying to create shipment with SMSA.");
			throw new SMSAShippingException(e.getMessage(), SMSAShippingExceptionType.REMOTE_ERROR);
		}
		return Optional.ofNullable(result);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.smsashipping.service.SMSAShippingService#createShipment(de.hybris. platform.core.model.order.
	 * Object, com.erabia.erabiacarrierbackoffice.model.Object)
	 */
	@Override
	public Optional<String> createShipment(final SMSAShipmentData smsaShipmentData)
	{
		if (smsaShipmentData == null)
		{
			throw new SMSAShippingException("smsaShipmentData is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getPassKey() == null)
		{
			throw new SMSAShippingException("passkey is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getCustomerName() == null)
		{
			throw new SMSAShippingException("customer name is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getCustomerCityName() == null)
		{
			throw new SMSAShippingException("city name is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}
		if (smsaShipmentData.getCustomerMobileNumber() == null)
		{
			throw new SMSAShippingException("Mobile number is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getCustomerAddressLine1() == null || smsaShipmentData.getCustomerAddressLine2() == null)
		{
			throw new SMSAShippingException("at least one shipment address shoudl be filled",
					SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getNumberOfItems() == null)
		{
			throw new SMSAShippingException("Number of items is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}

		if (smsaShipmentData.getShipmentType() == null)
		{
			throw new SMSAShippingException("Ship Type is null", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}
		smsaShipmentData.setItemDescription("");
		String result = null;

		try
		{
			result = smsaShippingConfig.getProxyService().addShipment(smsaShipmentData.getPassKey(), smsaShipmentData.getRefNo(),
					smsaShipmentData.getSentDate(), smsaShipmentData.getIdNo(), smsaShipmentData.getCustomerName(),
					smsaShipmentData.getCustomerCountryIsoCode(), smsaShipmentData.getCustomerCityName(),
					smsaShipmentData.getCustomerZipCode(), smsaShipmentData.getCustomerPOBox(),
					smsaShipmentData.getCustomerMobileNumber(), smsaShipmentData.getCustomerTelephoneNumber1(),
					smsaShipmentData.getCustomerTelephoneNumber2(), smsaShipmentData.getCustomerAddressLine1(),
					smsaShipmentData.getCustomerAddressLine2(), smsaShipmentData.getShipmentType(),
					smsaShipmentData.getNumberOfItems(), smsaShipmentData.getEmailAddress(), smsaShipmentData.getCarriageValue(),
					smsaShipmentData.getCarriageCurrency(), smsaShipmentData.getCodAmount(), smsaShipmentData.getWeight(),
					smsaShipmentData.getCustomsValue(), smsaShipmentData.getCustomsCurrency(), smsaShipmentData.getInsuranceAmount(),
					smsaShipmentData.getInsuranceCurrency(), smsaShipmentData.getItemDescription());
		}
		catch (final RemoteException e)
		{
			LOG.error("Exception occured while trying to create shipment with SMSA.");
			throw new SMSAShippingException(e.getMessage(), SMSAShippingExceptionType.REMOTE_ERROR);
		}

		LOG.info("Response returned from SMSA: " + result);
		return Optional.ofNullable(result);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.smsashipping.service.SMSAShippingService#getPDF(java.lang.String,
	 * com.erabia.erabiacarrierbackoffice.model.Object)
	 */
	@Override
	public Optional<byte[]> getPDF(final String awbNumber, final String passkey)
	{
		if (awbNumber == null)
		{
			LOG.error("Missing required data: awbNumber or Object");
			throw new SMSAShippingException("awbNumber or Object is null.", SMSAShippingExceptionType.MISSING_ARGUMENT);
		}
		byte[] pdfFile = null;
		try
		{
			pdfFile = smsaShippingConfig.getProxyService().getPDF(awbNumber, passkey);
		}
		catch (final RemoteException e)
		{
			LOG.error("Exception occured while trying to create shipment with SMSA.");
			throw new SMSAShippingException(e.getMessage(), SMSAShippingExceptionType.REMOTE_ERROR);
		}
		return Optional.ofNullable(pdfFile);
	}
}
