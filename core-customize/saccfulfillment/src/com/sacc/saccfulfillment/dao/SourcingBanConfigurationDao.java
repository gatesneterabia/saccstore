/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.dao;

import com.sacc.saccfulfillment.model.SourcingBanConfigModel;


/**
 * @author monzer
 */
public interface SourcingBanConfigurationDao
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
