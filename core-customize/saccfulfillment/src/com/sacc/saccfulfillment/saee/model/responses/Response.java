package com.sacc.saccfulfillment.saee.model.responses;

/**
 * @author Husam Dababneh
 */
public class Response
{
	private boolean success;
	private int error_code;
	private String error;

	public Response()
	{
		// TODO Auto-generated constructor stub
	}

	public Response(final boolean success, final int error_code, final String error)
	{
		super();
		this.success = success;
		this.error_code = error_code;
		this.error = error;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(final boolean success)
	{
		this.success = success;
	}

	public int getError_code()
	{
		return error_code;
	}

	public void setError_code(final int error_code)
	{
		this.error_code = error_code;
	}

	public String getError()
	{
		return error;
	}

	public void setError(final String error)
	{
		this.error = error;
	}

}
