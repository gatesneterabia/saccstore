/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.dao.SaeeUpdateNotificationRecordDao;
import com.sacc.saccfulfillment.model.SaeeUpdateNotificationRecordModel;
import com.sacc.saccfulfillment.service.SaeeUpdateNotificationRecordService;

/**
 * The Class DefaultSaeeUpdateNotificationRecordService.
 *
 * @author monzer
 */
public class DefaultSaeeUpdateNotificationRecordService implements SaeeUpdateNotificationRecordService
{

	/** The saee update notification record dao. */
	@Resource(name = "saeeUpdateNotificationRecordDao")
	private SaeeUpdateNotificationRecordDao saeeUpdateNotificationRecordDao;

	/**
	 * Creates the notification record.
	 *
	 * @required @param secretKey the secret key
	 * @required @param response the response
	 * @required @param trackingId the tracking id
	 * @required @param requestStatus the request status
	 * @required @param done the done
	 */
	@Override
	public void createNotificationRecord(final String requestBody, final String secretKey, final String response,
			final String trackingId,
			final String requestStatus, final boolean done, final Boolean authorized)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(requestBody), "requestBody is nul");
		Preconditions.checkArgument(StringUtils.isNotBlank(response), "response is nul");

		saeeUpdateNotificationRecordDao.createNotificationRecord(requestBody, secretKey, response, trackingId, requestStatus, done, authorized);

	}

	/**
	 * Find all notification records.
	 *
	 * @return the list
	 */
	@Override
	public List<SaeeUpdateNotificationRecordModel> findAllNotificationRecords()
	{
		final Optional<List<SaeeUpdateNotificationRecordModel>> allNotificationRecords = saeeUpdateNotificationRecordDao
				.findAllNotificationRecords();
		return allNotificationRecords.isEmpty() ? Collections.EMPTY_LIST : allNotificationRecords.get();
	}

	/**
	 * Find all notification records by query.
	 *
	 * @param secretKey
	 *           the secret key
	 * @param trackingId
	 *           the tracking id
	 * @param requestStatus
	 *           the request status
	 * @required @param done the done
	 * @return the list
	 */
	@Override
	public List<SaeeUpdateNotificationRecordModel> findAllNotificationRecordsByQuery(final String secretKey,
			final String trackingId, final String requestStatus, final boolean done, final Boolean authorized)
	{
		final Optional<List<SaeeUpdateNotificationRecordModel>> notificationRecordsByQuery = saeeUpdateNotificationRecordDao
				.findAllNotificationRecordsByQuery(secretKey, trackingId, requestStatus, done, authorized);
		return notificationRecordsByQuery.isEmpty() ? Collections.EMPTY_LIST : notificationRecordsByQuery.get();
	}

}
