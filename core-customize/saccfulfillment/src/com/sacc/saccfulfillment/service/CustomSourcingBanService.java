/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service;

import de.hybris.platform.warehousing.sourcing.ban.service.SourcingBanService;

import com.sacc.saccfulfillment.model.SourcingBanConfigModel;


/**
 *
 */
public interface CustomSourcingBanService extends SourcingBanService
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
