/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.allocation.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.allocation.impl.DefaultAllocationService;

import org.apache.log4j.Logger;

import com.sacc.saccfulfillment.service.allocation.CustomAllocationService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomAllocationService extends DefaultAllocationService implements CustomAllocationService
{
	protected static final Logger LOG = Logger.getLogger(DefaultCustomAllocationService.class);

	/**
	 * Creates the consignment entry.
	 *
	 * @param orderEntry
	 *           the order entry
	 * @param quantity
	 *           the quantity
	 * @param consignment
	 *           the consignment
	 * @return the consignment entry model
	 */
	@Override
	protected ConsignmentEntryModel createConsignmentEntry(final AbstractOrderEntryModel orderEntry, final Long quantity,
			final ConsignmentModel consignment)
	{
		final ConsignmentEntryModel entry = super.createConsignmentEntry(orderEntry, quantity, consignment);
		LOG.info("Weight: " + orderEntry.getProduct().getWeight() + " ,TotalWeight: "
				+ orderEntry.getProduct().getWeight() * quantity);
		entry.setWeight(orderEntry.getProduct().getWeight() * quantity);

		return entry;
	}
}
