/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccsmarteditcustomaddon.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class SaccsmarteditcustomaddonConstants extends GeneratedSaccsmarteditcustomaddonConstants
{
	public static final String EXTENSIONNAME = "saccsmarteditcustomaddon";
	
	private SaccsmarteditcustomaddonConstants()
	{
		//empty
	}
}
