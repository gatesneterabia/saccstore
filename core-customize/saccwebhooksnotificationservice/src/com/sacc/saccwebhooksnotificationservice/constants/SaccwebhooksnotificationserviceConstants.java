/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwebhooksnotificationservice.constants;

/**
 * Global class for all Saccwebhooksnotificationservice constants. You can add global constants for your extension into this class.
 */
public final class SaccwebhooksnotificationserviceConstants extends GeneratedSaccwebhooksnotificationserviceConstants
{
	public static final String EXTENSIONNAME = "saccwebhooksnotificationservice";

	private SaccwebhooksnotificationserviceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccwebhooksnotificationservicePlatformLogo";
}
