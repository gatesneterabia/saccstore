package com.sacc.saccwebhooksnotificationservice.saee.entry;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


public class NotificationRequest implements Serializable
{

	@JsonProperty(value = "event")
	private String event;

	@JsonProperty(value = "message")
	private NotificationMessage message;

	@JsonProperty(value = "status")
	private String status;

	/**
	 *
	 */
	public NotificationRequest()
	{
		// XXX Auto-generated constructor stub
	}

	/**
	 *
	 */
	public NotificationRequest(final String event, final NotificationMessage message, final String status)
	{
		super();
		this.event = event;
		this.message = message;
		this.status = status;
	}

	public String getEvent()
	{
		return event;
	}

	public void setEvent(final String event)
	{
		this.event = event;
	}

	public NotificationMessage getMessage()
	{
		return message;
	}

	public void setMessage(final NotificationMessage message)
	{
		this.message = message;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return " [event=" + event + ", message=" + message + ", status=" + status + "]";
	}

}
