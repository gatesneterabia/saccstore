/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.records;

import java.util.List;

import com.sacc.saccpayment.model.PaymentTransactionRecordsModel;


/**
 *
 */
public interface PaymentTransactionRecordsDao
{

	void createPaymentRecord(PaymentTransactionRecordsModel model);

	List<PaymentTransactionRecordsModel> getAllPaymentRecords();

	List<PaymentTransactionRecordsModel> getAllPaymentRecordsForQuery(PaymentTransactionRecordsModel model);

}
