package com.sacc.saccpayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.hyperpay.enums.ShippingMethod;
import com.sacc.saccpayment.hyperpay.enums.TestMode;
import com.sacc.saccpayment.hyperpay.enums.TransactionStatus;
import com.sacc.saccpayment.hyperpay.exception.HyperpayPaymentException;
import com.sacc.saccpayment.hyperpay.model.HyperpayPaymentData;
import com.sacc.saccpayment.hyperpay.service.HyperpayService;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.service.records.PaymentTransactionRecordService;
import com.sacc.saccpayment.strategy.CustomPaymentTransactionStrategy;
import com.sacc.saccpayment.strategy.PaymentStrategy;


/**
 * The Class DefaultHyperpayPaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultHyperpayPaymentStrategy.
 */
public class DefaultHyperpayPaymentStrategy implements PaymentStrategy
{

	/** The Constant CITY_STATE. */
	private static final String CITY_STATE = "Riyadh";

	/** The Constant STREET_1. */
	private static final String STREET_1 = "Prince Sultan Road - Al Muhammadiyah District 5";

	/** The default Post code. */
	private static final String POSTCODE = "11564";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultHyperpayPaymentStrategy.class);
	/** The Constant DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY. */
	private static final String DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY = "Delivery Address can not be empty";


	/** The Constant PAYMENT_ADDRESS_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_ADDRESS_CAN_NOT_BE_NULL = "Payment address can not be null";


	/** The Constant THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL. */
	private static final String THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL = "the provider moder is not a HyperpayPaymentProviderModel ";


	/** The Constant PAYMENT_INFO_DATA_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_INFO_DATA_CAN_NOT_BE_NULL = "paymentInfoData can not be null";

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	/** The payment provider service. */
	@Resource(name = "hyperpayService")
	private HyperpayService hyperpayService;


	/** The cart service. */
	@Resource(name = "cartService")
	private CartService cartService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The create subscription result converter. */
	@Resource(name = "hyperpayCreateSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/** The payment transaction record service. */
	@Resource(name = "paymentTransactionRecordService")
	private PaymentTransactionRecordService paymentTransactionRecordService;

	/**
	 * Gets the creates the subscription result converter.
	 *
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}



	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";


	/** The Constant ABSTRACTORDER_MUSTN_T_BE_NULL. */
	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";
	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data mustn't be null";

	/** The Constant ID_CAN_NOT_BE_NULL. */
	private static final String ID_CAN_NOT_BE_NULL = "id mustn't be null";

	/** The key generator. */
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;


	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 */
	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel, ABSTRACTORDER_MUSTN_T_BE_NULL);

		LOG.info("DefaultHyperpayPaymentStrategy : Building the Payment Request Data to prepare the checkout by Hyperpay");
		final HyperpayPaymentData requestData = getRequestData(paymentProviderModel, abstractOrder);

		try
		{
			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;


			final Map<String, Object> prepareCheckout = hyperpayService.prepareCheckout(
					hyperpayPaymentProviderModel.getAccessToken(), hyperpayPaymentProviderModel.getEntityId(),
					hyperpayPaymentProviderModel.getCheckoutsURL(), requestData);

			if (prepareCheckout != null && prepareCheckout.get("id") != null)
			{
				paymentTransactionRecordService.savePaymentRecords("PREPARE_CHECKOUT", extractResultDescription(prepareCheckout),
						requestData.toString(),
						prepareCheckout.toString(),
						extractResponseCode(prepareCheckout), abstractOrder);
				final String id = (String) prepareCheckout.get("id");
				LOG.info("DefaultHyperpayPaymentStrategy : Checkout prepared successfully with checkoutId : " + id);
				modelService.refresh(abstractOrder);
				abstractOrder.setRequestPaymentBody(requestData.toString());
				abstractOrder.setResponsePaymentBody(prepareCheckout.toString());
				LOG.info("DefaultHyperpayPaymentStrategy : Checkout response : " + prepareCheckout.toString());
				modelService.save(abstractOrder);
				final PaymentRequestData paymentRequestData = new PaymentRequestData(hyperpayPaymentProviderModel.getSrcForm() + id,
						HyperpayPaymentProviderModel._TYPECODE, hyperpayPaymentProviderModel);
				return Optional.ofNullable(paymentRequestData);

			}
		}
		catch (final PaymentException e)
		{
			LOG.error(this.getClass() + " : " + e.getMessage(), e);
			paymentTransactionRecordService.savePaymentRecords("PREPARE_CHECKOUT", "NOT_PREPARED", requestData.toString(),
					e.toString(), "FAILED",
					abstractOrder);
			return Optional.empty();
		}


		return Optional.empty();
	}

	/**
	 * Generate order code.
	 *
	 * @return the string
	 */
	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the optional
	 */
	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		LOG.info("DefaultHyperpayPaymentStrategy : Building Payment Response Data via PaymentCheckoutStatus");

		final String id = (String) data;

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		Map<String, Object> checkoutStatus = null;
		try
		{
			checkoutStatus = hyperpayService.getCheckoutStatus(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getCheckoutsURL(), id);

			LOG.info("DefaultHyperpayPaymentStrategy : PaymentCheckoutStatus response is : " + checkoutStatus.toString());
			paymentTransactionRecordService.savePaymentRecords("GET_PAYMENT_STATUS", extractResultDescription(checkoutStatus),
					"[PaymentId : ]" + id,
					checkoutStatus.toString(),
					extractResponseCode(checkoutStatus), abstractOrder);
			if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get("id") != null)
			{
				abstractOrder.setPaymentReferenceId((String) checkoutStatus.get("id"));
				modelService.save(abstractOrder);

				final PaymentResponseStatus paymentResponseStatus = isSuccessfullPaymnetOrder(checkoutStatus);
				LOG.info("DefaultHyperpayPaymentStrategy : Payment Response Status is : " + paymentResponseStatus);

				final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
								? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
								: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status is : " + transactionStatus);

				final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
								: TransactionStatusDetails.REVIEW_NEEDED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status Details is : " + transactionStatusDetails);

				final String requestPaymentBody = "Payment Id= [" + abstractOrder.getPaymentReferenceId() + "]";
				final String responsePaymentBody = checkoutStatus.toString();
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
						responsePaymentBody, null);

				modelService.refresh(abstractOrder);
				abstractOrder.setResponsePaymentBody(checkoutStatus.toString());
				modelService.save(abstractOrder);
				final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
						HyperpayPaymentProviderModel._TYPECODE, null);
				return Optional.ofNullable(paymentResponseData);
			}
			else
			{
				LOG.error("DefaultHyperpayPaymentStrategy : checkout response is empty or does nothave an id");
				final String requestPaymentBody = "Payment Id= [" + id + "]";
				final String responsePaymentBody = checkoutStatus.toString();

				paymentTransactionRecordService.savePaymentRecords("GET_PAYMENT_STATUS", extractResultDescription(checkoutStatus),
						"[PaymentId : ]" + id,
						checkoutStatus.toString(),
						extractResponseCode(checkoutStatus), abstractOrder);

				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
						"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null);

				return Optional.empty();
			}
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = "Payment Id= [" + id + "]";
			final String responsePaymentBody = checkoutStatus.toString();

			paymentTransactionRecordService.savePaymentRecords("GET_PAYMENT_STATUS", extractResultDescription(checkoutStatus),
					"[PaymentId : " + id + "]",
					ExceptionUtils.getStackTrace(e),
					extractResponseCode(checkoutStatus), abstractOrder);

			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null);
			LOG.error("DefaultHyperpayPaymentStrategy : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	/**
	 * To digits.
	 *
	 * @param d
	 *           the d
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String toDigits(final double d, final CurrencyModel currency)
	{
		final int digits = currency.getDigits() == null ? 0 : currency.getDigits().intValue();
		final String value = String.format("%." + digits + "f", d);
		return value;
	}

	/**
	 * Round.
	 *
	 * @param num
	 *           the num
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String round(final double num, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return "" + num;
		}
		final int digits = currency.getDigits() == null ? 0 : currency.getDigits().intValue();
		BigDecimal decimal = new BigDecimal(num);
		decimal = decimal.setScale(digits, RoundingMode.HALF_UP);
		return decimal.toString();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the order
	 * @return the response data
	 */
	protected HyperpayPaymentData getRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel,
				THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL);
		Preconditions.checkArgument(commonI18NService.getCurrentLanguage() != null, "CurrentLanguage is null");

		if (StringUtils.isBlank(abstractOrderModel.getOrderCode()))
		{
			abstractOrderModel.setOrderCode(generateOrderCode());
			modelService.save(abstractOrderModel);
		}

		final HyperpayPaymentProviderModel providerModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final AddressModel paymentAddress = abstractOrderModel.getPaymentAddress();
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		final HyperpayPaymentData data = new HyperpayPaymentData();

		final Double totalPrice = getTotalPrice(abstractOrderModel);
		final Double taxAmount = getTaxAmountPrice(abstractOrderModel);
		if (providerModel.isRound())
		{
			data.setAmount(round(totalPrice.doubleValue(), providerModel.getCurrency()));
			data.setTaxAmount(round(taxAmount.doubleValue(), providerModel.getCurrency()));
		}
		else
		{
			data.setAmount((totalPrice == null ? null : toDigits(totalPrice.doubleValue(), providerModel.getCurrency())));
			data.setTaxAmount((taxAmount == null ? null : toDigits(taxAmount.doubleValue(), providerModel.getCurrency())));
		}

		data.setMerchantTransactionId(abstractOrderModel.getOrderCode());
		final CustomerModel customerModel = (de.hybris.platform.core.model.user.CustomerModel) abstractOrderModel.getUser();
		data.setEntityId(providerModel.getEntityId());

		data.setCurrency(providerModel.getCurrency().getIsocode());
		data.setPaymentType(providerModel.getPaymentType());
		final boolean round = providerModel.isRound();
		if (round)
		{
			data.setTestMode(TestMode.INTERNAL.toString().equalsIgnoreCase(providerModel.getTestMode()) ? TestMode.INTERNAL
					: TestMode.EXTERNAL);
		}
		data.setCustomerMerchantCustomerId(customerModel.getCustomerID());
		data.setCustomerGivenName(
				StringUtils.isNotBlank(customerModel.getDisplayName()) ? customerModel.getDisplayName().toUpperCase().charAt(0) + ""
						: null);
		data.setCustomerSurname(
				StringUtils.isNotBlank(customerModel.getDisplayName()) && customerModel.getDisplayName().length() >= 2
						&& customerModel.getDisplayName().charAt(1) != ' ' ? customerModel.getDisplayName().toUpperCase().charAt(1) + ""
								: customerModel.getDisplayName().toUpperCase().charAt(0) + "");

		data.setCustomerPhone(customerModel.getMobileNumber());
		data.setCustomerMobile(customerModel.getMobileNumber());
		data.setCustomerWorkPhone(customerModel.getMobileNumber());
		data.setCustomerEmail(customerModel.getContactEmail());

		if (paymentAddress != null)
		{
			data.setBillingPostCode(POSTCODE);
			data.setBillingCountry(paymentAddress.getCountry().getIsocode());
			data.setBillingStreet1(STREET_1);
			data.setBillingStreet2(paymentAddress.getLine2());
			data.setBillingCity(CITY_STATE);
			data.setBillingState(CITY_STATE);
		}
		if (deliveryAddress != null)
		{
			data.setShippingStreet1(deliveryAddress.getLine1());
			data.setShippingStreet2(deliveryAddress.getLine1());
			data.setShippingPostCode("00000");
			data.setShippingCountry(deliveryAddress.getCountry().getIsocode());
			data.setShippingMethod(ShippingMethod.OTHER);
			if (providerModel.isRound())
			{
				data.setShippingCost(round(abstractOrderModel.getDeliveryCost(), providerModel.getCurrency()));
			}
			else
			{
				data.setShippingCost((abstractOrderModel.getDeliveryCost() == null ? null
						: toDigits(abstractOrderModel.getDeliveryCost().doubleValue(), providerModel.getCurrency())));
			}
		}

		return data;
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		CurrencyModel currency = null;
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			currency = baseCurrency;
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		return amount;
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTaxAmountPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		CurrencyModel currency = null;
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalTax();
		}
		else
		{
			currency = baseCurrency;
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		return amount;
	}



	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final String id = (String) data;
		final PaymentResponseStatus responseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
				hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);

		return PaymentResponseStatus.SUCCESS.equals(responseStatus);
	}

	/**
	 * Gets the payment order status response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final String id = data == null ? abstractOrder.getPaymentReferenceId() : (String) data;
		Preconditions.checkArgument(id != null, ID_CAN_NOT_BE_NULL);

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		Map<String, Object> checkoutStatus = null;
		try
		{
			checkoutStatus = hyperpayService.searchForTransactionById(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);

			if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get("id") != null)
			{
				abstractOrder.setPaymentReferenceId((String) checkoutStatus.get("id"));
				modelService.save(abstractOrder);

				paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY",
						extractResultDescription(checkoutStatus), "[PaymentId : " + id + "]", checkoutStatus.toString(),
						extractResponseCode(checkoutStatus), abstractOrder);

				final PaymentResponseStatus paymentResponseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
						hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(),
						abstractOrder.getPaymentReferenceId());
				LOG.info("DefaultHyperpayPaymentStrategy : Payment Response Status By Payment Id "
						+ abstractOrder.getPaymentReferenceId() + " is : " + paymentResponseStatus);

				final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
								? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
								: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status By Payment Id " + abstractOrder.getPaymentReferenceId()
						+ " is : " + transactionStatus);

				final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
						|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
								: TransactionStatusDetails.REVIEW_NEEDED;
				LOG.info("DefaultHyperpayPaymentStrategy : Transaction Status Details By Payment Id "
						+ abstractOrder.getPaymentReferenceId() + " is : " + transactionStatusDetails);

				final String requestPaymentBody = "Payment Id= [" + abstractOrder.getPaymentReferenceId() + "]";
				final String responsePaymentBody = checkoutStatus.toString();

				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
						responsePaymentBody, null);

				modelService.refresh(abstractOrder);
				abstractOrder.setResponsePaymentBody(checkoutStatus.toString());
				modelService.save(abstractOrder);

				final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
						HyperpayPaymentProviderModel._TYPECODE, null);
				LOG.info("DefaultHyperpayPaymentStrategy : transaction Response Data is : " + paymentResponseData);
				return Optional.ofNullable(paymentResponseData);
			}
			else
			{
				LOG.error(
						"DefaultHyperpayPaymentStrategy : Transaction Response Data By Payment Id is failed due to empty response ormissing Id");
				paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY",
						extractResultDescription(checkoutStatus), "[PaymentId : " + id + "]",
						checkoutStatus.toString(),
						extractResponseCode(checkoutStatus), abstractOrder);
				final String requestPaymentBody = "Payment Id= [" + id + "]";
				final String responsePaymentBody = checkoutStatus.toString();
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
						"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null);

				return Optional.empty();
			}
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = "Payment Id= [" + id + "]";
			final String responsePaymentBody = checkoutStatus.toString();

			paymentTransactionRecordService.savePaymentRecords("GET_TRANSACTION_HISTORY", extractResultDescription(checkoutStatus),
					"[PaymentId : " + id + "]",
					ExceptionUtils.getStackTrace(e),
					extractResponseCode(checkoutStatus), abstractOrder);

			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null);
			LOG.error("DefaultHyperpayPaymentStrategy : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	/**
	 * Capture order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Cancel order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Gets the response status.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param queryURL
	 *           the query URL
	 * @param paymentId
	 *           the payment id
	 * @return the response status
	 */
	private PaymentResponseStatus getResponseStatus(final String accessToken, final String entityId, final String queryURL,
			final String paymentId)
	{
		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(accessToken, entityId, queryURL, paymentId))
				? PaymentResponseStatus.SUCCESS
				: PaymentResponseStatus.FAILURE;
	}

	private PaymentResponseStatus isSuccessfullPaymnetOrder(final Map<String, Object> response)
	{
		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(response)) ? PaymentResponseStatus.SUCCESS
				: PaymentResponseStatus.FAILURE;
	}

	/**
	 * Checks if is success payment.
	 *
	 * @param providerModel
	 *           the provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the payment response status
	 */
	private PaymentResponseStatus isSuccessPayment(final PaymentProviderModel providerModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		try
		{
			final Optional<PaymentResponseData> statusResponseData = getPaymentOrderStatusResponseData(providerModel, abstractOrder, data);
			if(statusResponseData.isEmpty()) {
				return PaymentResponseStatus.FAILURE;
			}
			return PaymentResponseStatus.SUCCESS
					.equals(statusResponseData.get().getStatus())
							? PaymentResponseStatus.SUCCESS
							: PaymentResponseStatus.FAILURE;
		}
		catch (final PaymentException e)
		{
			LOG.error("DefaultHyperpayPaymentStrategy : " + e.getMessage(), e);
			return PaymentResponseStatus.FAILURE;
		}
	}

	/**
	 * Refund order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Extract response code.
	 *
	 * @param response
	 *           the response
	 * @return the string
	 */
	private String extractResponseCode(final Map<String, Object> response)
	{
		return response.get("result") != null && ((Map<String, Object>) response.get("result")).get("code") != null
				? String.valueOf(((Map<String, Object>) response.get("result")).get("code"))
				: "FAILED";
	}

	/**
	 * Extract result description.
	 *
	 * @param response
	 *           the response
	 * @return the string
	 */
	private String extractResultDescription(final Map<String, Object> response)
	{
		return response.get("result") != null && ((Map<String, Object>) response.get("result")).get("description") != null
				? String.valueOf(((Map<String, Object>) response.get("result")).get("description"))
				: "FAILED";
	}


	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		if(abstractOrderModel == null) {
			return false;
		}
		if (StringUtils.isBlank(abstractOrderModel.getPaymentReferenceId()))
		{
			return false;
		}
		if(CollectionUtils.isEmpty(abstractOrderModel.getPaymentTransactions())) {
			return false;
		}
		for (final PaymentTransactionModel transaction : abstractOrderModel.getPaymentTransactions())
		{
			LOG.info("THE SAME TRANSACTION? : " + transaction.getRequestId().equals(abstractOrderModel.getPaymentReferenceId()));
			if (transaction.getRequestId().equals(abstractOrderModel.getPaymentReferenceId()))
			{
				final Optional<PaymentTransactionEntryModel> successTransaction = transaction.getEntries().stream()
						.filter(entry -> de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED.name()
								.equals(entry.getTransactionStatus()) && PaymentTransactionType.REVIEW_DECISION.equals(entry.getType()))
						.findAny();
				if (successTransaction.isEmpty())
				{
					return false;
				}
				return true;
			}
		}
		return false;
	}

}
