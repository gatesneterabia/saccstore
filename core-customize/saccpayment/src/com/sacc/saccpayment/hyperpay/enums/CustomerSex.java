package com.sacc.saccpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum CustomerSex {
	
	M, F;
	
}
