package com.sacc.saccpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum TestMode {
	EXTERNAL, INTERNAL
}
