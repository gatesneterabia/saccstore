package com.sacc.saccpayment.hyperpay.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

/**
 * The Interface HyperpayPaymentHttpsConnectionService.
 */
public interface HyperpayPaymentHttpsConnectionService {


	/**
	 * Send request.
	 *
	 * @param body the body
	 * @param requestUrl the request url
	 * @param requestMethod the http request method
	 * @return the input stream returned from the response
	 * @throws MalformedURLException the malformed URL exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws IllegalAccessException the illegal access exception
	 */
	InputStream sendRequest(final Object body, final String requestUrl, final String requestMethod, final String accessToken)
			throws MalformedURLException, IOException, IllegalAccessException;


}
