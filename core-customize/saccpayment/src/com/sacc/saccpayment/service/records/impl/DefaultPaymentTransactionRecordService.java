/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.service.records.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.dao.records.PaymentTransactionRecordsDao;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentTransactionRecordsModel;
import com.sacc.saccpayment.service.records.PaymentTransactionRecordService;

/**
 *
 */
public class DefaultPaymentTransactionRecordService implements PaymentTransactionRecordService
{

	@Resource(name = "paymentTransactionRecordsDao")
	private PaymentTransactionRecordsDao paymentTransactionRecordsDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void savePaymentRecords(final String action, final String paymentStatus, final String request, final String response,
			final String resultCode, final AbstractOrderModel abstractOrder)
	{
		Preconditions.checkArgument(abstractOrder != null, "abstractOrderModel is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(abstractOrder.getOrderCode()), "Order Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(abstractOrder.getGuid()), "Cart Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(resultCode), "Result Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(abstractOrder.getUser().getUid()), "Use Email is null");

		final PaymentTransactionRecordsModel model = modelService.create(PaymentTransactionRecordsModel.class);
		model.setAction(action);
		model.setOrderCode(abstractOrder.getOrderCode());

		model.setCartCode(abstractOrder.getGuid());
		model.setAmount(abstractOrder.getTotalPrice().toString());
		model.setPaymentId(abstractOrder.getPaymentReferenceId());
		model.setPaymentTransactionCodes(abstractOrder.getPaymentTransactions().stream().map(transaction -> transaction.getCode())
				.collect(Collectors.toList()));
		model.setPaymentInfoId(abstractOrder.getPaymentInfo() != null && abstractOrder.getPaymentInfo().getCode() != null
				? abstractOrder.getPaymentInfo().getCode()
				: "NOT_SPECIFIED");
		model.setProvider(HyperpayPaymentProviderModel._TYPECODE);
		model.setUserEmail(abstractOrder.getUser().getUid());
		model.setPaymentStatus(paymentStatus);
		model.setOrderEntries(
				abstractOrder.getEntries().stream().map(entry -> entry.getProduct().getCode()).collect(Collectors.toList()));
		model.setRequestBody(request);
		model.setResponseBody(response);
		model.setResultCode(resultCode);

		modelService.save(model);
	}

	@Override
	public List<PaymentTransactionRecordsModel> getAllPaymentRecords()
	{
		return paymentTransactionRecordsDao.getAllPaymentRecords();
	}

	@Override
	public List<PaymentTransactionRecordsModel> getAllPaymentRecordsByFilter(final PaymentTransactionRecordsModel model)
	{
		Preconditions.checkArgument(model != null, "Payment Record Model is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getOrderCode()), "Order Code is null");

		return paymentTransactionRecordsDao.getAllPaymentRecordsForQuery(model);
	}

}
