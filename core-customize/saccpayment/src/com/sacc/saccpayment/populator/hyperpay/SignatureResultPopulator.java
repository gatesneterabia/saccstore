/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;

import java.math.BigDecimal;
import java.util.Map;

import com.sacc.saccpayment.hyperpay.HyperpayStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class SignatureResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final BigDecimal amount = getAmount(source);

		final String currency = String.valueOf(source.get(HyperpayStatusReason.CURRENCY.getKey()));
		final String merchantID = "";
		final String orderPageSerialNumber = null;
		final String sharedSecret = null;
		final String orderPageVersion = "1.1";
		final String amountPublicSignature = null;
		final String currencyPublicSignature = String.valueOf(source.get(HyperpayStatusReason.CURRENCY.getKey()));
		final String transactionSignature = String.valueOf(source.get(HyperpayStatusReason.ID.getKey()));
		final String signedFields = null;

		final SignatureData data = new SignatureData();

		data.setAmount(amount);
		data.setCurrency(currency);
		data.setMerchantID(merchantID);
		data.setOrderPageSerialNumber(orderPageSerialNumber);
		data.setSharedSecret(sharedSecret);
		data.setOrderPageVersion(orderPageVersion);
		data.setAmountPublicSignature(amountPublicSignature);
		data.setCurrencyPublicSignature(currencyPublicSignature);
		data.setTransactionSignature(transactionSignature);
		data.setSignedFields(signedFields);

		target.setSignatureData(data);
	}

	private BigDecimal getAmount(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.AMOUNT.getKey());
		if (object == null)
		{
			return null;
		}
		try
		{
			return BigDecimal.valueOf(Double.valueOf((String) object));
		}
		catch (final java.lang.NumberFormatException e)
		{
			return null;
		}
	}
}
