<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>

<spring:htmlEscape defaultHtmlEscape="true" />
<cms:pageSlot position="TopHeaderSlot" var="component" element="div" >
	<cms:component component="${component}" />
</cms:pageSlot>
<div class="row no-margin headtop hidden-md hidden-lg">

					
					<div class="col-xs-12 col-sm-12   text-center">
					<cms:pageSlot position="TopAlert" var="component" element="div" class="owl-carousel carousel js-owl-carousel owl-carousel js-owl-top-text owl-theme">
					<cms:component component="${component}" />
					</cms:pageSlot>
					</div>
					
					<div class="col-xs-3 col-sm-3 hidden">
						<ul class="top-links">

							
							
							
							
					</ul>
					
		</div>
		</div>
<div class="branding-mobile hidden-md hidden-lg">
			<a class="js-toggle-sm-navigation color_icon_nav_mobile pull-left" href="javascript:;">
									<i class="far fa-bars"></i>
								</a>
			<div class="js-mobile-logo pull-left">
				<%--populated by JS acc.navigation--%>
			</div>
			<div class="pull-right">
					
							<c:url value="/my-account/my-wishlist" var="wishlist"/>
									<a href="${wishlist}" class="btn_wishlist"></a>		
							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer">
								<cms:component component="${cart}" element="div" class="color_icon_nav_mobile" />
							</cms:pageSlot>
							
									
								
							</div>
			<div class="site-search active">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
			
		</div>


<header class="js-mainHeader">
	<nav class="navigation navigation--top">
		<div class="row no-margin hidden-xs hidden-sm">
			<div class="col-sm-12 col-md-12 head">
					
				
					<div class="row">
					
					<div class="col-xs-12 col-md-12   text-center">
					<cms:pageSlot position="TopAlert" var="component" element="div" >
					<cms:component component="${component}" />
					</cms:pageSlot>
					</div>
					
					<div class="col-xs-3 col-md-4 hidden">
						<ul class="top-links">

							
							
							
							
					</ul>
					
</div>
					
				
				
			</div>
		
		</div>
		</div>
		<div class="row no-margin hidden-xs hidden-sm">
			<div class="col-xs-3 pull-left col-lg-3 col-md-3 hidden-xs hidden-sm">
			
				<div class="nav__left js-site-logo">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot>
				</div>
			</div>
			<div class="col-xs-5  col-md-4 col-lg-5 col-xs-12 col-sm-12">
			<div class="site-search">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
						</div>	
					<div class="col-xs-4 pull-right col-lg-4 col-md-5 hidden-xs hidden-sm">
					
					<ul class="nav__links nav__links--shop_info links_com_myC ">
						<li>
									<c:url value="/my-account/my-wishlist" var="wishlist"/>
									<a href="${wishlist}" class="btn_wishlist"></a>
								</li>
						
						<li> 
							<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
								<cms:component component="${cart}" element="div"/>
							</cms:pageSlot>
						</li>
						
						<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas accountlogin">
									<i class="userimage"></i>
									<ycommerce:testId code="header_Login_link">
										<c:url value="/login" var="loginUrl" />
										<c:url value="/register" var="registerUrl" />
										<a href="${fn:escapeXml(loginUrl)}">
											<spring:theme code="header.mobile.link.login" />
										</a>
										<a href="${fn:escapeXml(registerUrl)}">
											<spring:theme code="header.mobile.link.register" />
										</a>
										<span><spring:theme code="header.link.account" /></span>
									</ycommerce:testId>
								</li>
								<li class="liOffcanvas">
								<ycommerce:testId code="header_Login_link">
								<cms:pageSlot position="LoginPopup" var="component" limit="1">
							 	<cms:component component="${component}" />
							</cms:pageSlot>
							</ycommerce:testId>
							</li>
							</sec:authorize>
						 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" />
							 </cms:pageSlot>

						
					<li class="lang">	
					<header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/> 	
					</li>
				

							
							

							
						
								

								
						
					</ul>
						</div>
		<!--  	<div class="col-sm-12 col-md-8" style="display:none">
				<div class="nav__right">
					<ul class="nav__links nav__links--account">
						<c:if test="${empty hideHeaderLinks}">
							<c:if test="${uiExperienceOverride}">
								<li class="backToMobileLink">
									<c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
									<a href="${fn:escapeXml(backToMobileStoreUrl)}">
										<spring:theme code="text.backToMobileStore" />
									</a>
								</li>
							</c:if>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="logged_in js-logged_in">
									<ycommerce:testId code="header_LoggedUser">
										<spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" />
									</ycommerce:testId>
								</li>
							</sec:authorize>

							 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" />
							 </cms:pageSlot>

							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_Login_link">
										<c:url value="/login" var="loginUrl" />
										<a href="${fn:escapeXml(loginUrl)}">
											<spring:theme code="header.link.login" />
										</a>
									</ycommerce:testId>
								</li>
								<li class="liOffcanvas">
								<ycommerce:testId code="header_Login_link">
								<cms:pageSlot position="LoginPopup" var="component" limit="1">
							 	<cms:component component="${component}" />
							</cms:pageSlot>
							</ycommerce:testId>
							</li>
							</sec:authorize>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_signOut">
										<c:url value="/logout" var="logoutUrl"/>
										<a href="${fn:escapeXml(logoutUrl)}">
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>
							
						</c:if>
					</ul>
				</div>
			</div>
			 -->
		</div>
	</nav>
	
	<%-- a hook for the my account links in desktop/wide desktop--%>
	<div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
		<ul class="nav__links">

		</ul>
	</div>

	<div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
		<ul class="nav__links js-nav__links">

		</ul>
	</div>
	
	<nav class="navigation navigation--middle js-navigation--middle hidden">
		<div class="container-fluid">
			<div class="row">
				<div class="mobile__nav__row mobile__nav__row--table">
					<div class="mobile__nav__row--table-group">
						<div class="mobile__nav__row--table-row">
							<div class="mobile__nav__row--table-cell visible-xs hidden-sm">
								<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation color_icon_nav_mobile"
										type="button">
									<span class="far fa-align-justify"></span>
								</button>
							</div>

							<div class="mobile__nav__row--table-cell visible-xs visible-sm mobile__nav__row--seperator ">
								<ycommerce:testId code="header_search_activation_button">
									<button	class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-md hidden-lg color_icon_nav_mobile"" type="button">
										<span class="far fa-search"></span>
									</button>
								</ycommerce:testId>
							</div>

							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator ">
										<c:url value="/login" var="login"/>
										<a href="${fn:escapeXml(login)}" class="mobile__nav__row--btn mobile__nav__row--btn-location btn color_icon_nav_mobile"">
											<span class="far fa-user"></span>
										</a>
									</div>
								</ycommerce:testId>
							</sec:authorize>
							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator ">
										<c:url value="/logout" var="logout"/>
										<a href="${fn:escapeXml(logout)}" class="mobile__nav__row--btn mobile__nav__row--btn-location btn color_icon_nav_mobile"">
											<span class="far fa-lock"></span>
										</a>
									</div>
								</ycommerce:testId>
							</sec:authorize>

							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg color_icon_nav_mobile">
								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
							</cms:pageSlot>

						</div>
					</div>
				</div>
			</div>
		<!-- 	<div class="row desktop__nav">
				<div class="nav__left col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-sm-2 hidden-xs visible-sm mobile-menu">
							<button class="btn js-toggle-sm-navigation" type="button">
								<span class="far fa-align-justify"></span>
							</button>
						</div>
						<div class="col-sm-10">
							<div class="site-search">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div>
				<div class="nav__right col-xs-6 col-xs-6 hidden-xs">
					<ul class="nav__links nav__links--shop_info">
						<li>
							<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
								<cms:component component="${cart}" element="div"/>
							</cms:pageSlot>
						</li>
					</ul>
				</div>
			</div>
			-->
		</div>
	</nav>
	
	<a id="skiptonavigation"></a>
	<nav:topNavigation />
</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>
