<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
<div class="se-pre-con">

<div class="loader loader2">
  <span style="--i:1;"></span>
  <span style="--i:2;"></span>
  <span style="--i:3;"></span>
  <span style="--i:4;"></span>
  <span style="--i:5;"></span>
  <span style="--i:6;"></span>
  <span style="--i:7;"></span>
  <span style="--i:8;"></span>
  <span style="--i:9;"></span>
  <span style="--i:10;"></span>
  <span style="--i:11;"></span>
  <span style="--i:12;"></span>
  <span style="--i:13;"></span>
  <span style="--i:14;"></span>
  <span style="--i:15;"></span>
  <span style="--i:16;"></span>
  <span style="--i:17;"></span>
  <span style="--i:18;"></span>
  <span style="--i:19;"></span>
  <span style="--i:20;"></span>
</div>

</div>
<div class="row no-margin">
<div class="col-md-8 col-sm-12 no-space">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
    <cms:pageSlot position="Section3" var="feature" element="div" class="catimages " >
        <cms:component component="${feature}" element="div" class=" row no-margin" />
    </cms:pageSlot>
</div>
<div class="col-md-4 col-sm-12">
 <cms:pageSlot position="Section2" var="feature" element="div" class="row no-magrin" >
        <cms:component component="${feature}" element="div" class="col-md-12 no-space"  />
    </cms:pageSlot>
</div>
</div>
   

    
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12">
    <cms:pageSlot position="Section4" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
	</div>
</div>
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12">
    <cms:pageSlot position="Section5" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
    </div>
</div> 
<div class="row  no-margin">
<div class="col-md-12 col-sm-12 no-space">  
     <cms:pageSlot position="Section6" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin lightback">
<div class="col-md-12 col-sm-12"> 
<cms:pageSlot position="Section9" var="feature" >
        <cms:component component="${feature}" element="h2" class="carousel__component--headline" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin lightback"> 
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section7" var="feature" element="div" class="row" >
        <cms:component limit="0" component="${feature}" element="div" class=" col-md-12 owl-carousel carousel js-owl-carousel owl-carousel js-owl-default-3 owl-theme no-space" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section8" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>

<div class="row padd30  no-margin lightback">
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section10" var="feature" element="div" class="row padd30" >
        <cms:component component="${feature}" element="div" class=" col-md-12 owl-carousel carousel js-owl-carousel owl-carousel js-owl-default-3 owl-theme no-space" />
    </cms:pageSlot>
</div>
</div>
    
<div class="row padd30  no-margin">
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section11" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>
<div class="row no-margin">
<div class="col-md-12 col-sm-12 no-space"> 
    <cms:pageSlot position="Section12" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
</div>
</div>
<div class="row padd30  no-margin lightback">
<div class="col-md-12 col-sm-12"> 
    <cms:pageSlot position="Section13" var="feature" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
  </div>
  </div>  
  <div class="row padd30 blackback  no-margin">
<div class="col-md-12 col-sm-12">
<div class="row list-img">
     <cms:pageSlot position="Section14" var="feature" element="div" class="col-md-3 col-sm-6 col-xs-6" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
    
        <cms:pageSlot position="Section15" var="feature" element="div" class="col-md-3 col-sm-6 col-xs-6" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
    
        <cms:pageSlot position="Section16" var="feature" element="div" class="col-md-3 col-sm-6 col-xs-6" >
        <cms:component component="${feature}" />
    </cms:pageSlot>
    
    <cms:pageSlot position="Section17" var="feature" element="div" class="col-md-3 col-sm-6 col-xs-6" >
        <cms:component component="${feature}" />
    </cms:pageSlot>


     </div>
  </div>  
  </div>

</template:page>
