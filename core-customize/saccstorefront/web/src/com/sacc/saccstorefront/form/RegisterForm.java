package com.sacc.saccstorefront.form;

import de.hybris.platform.acceleratorstorefrontcommons.forms.ConsentForm;


/**
 * Form object for registration.
 *
 * @author mnasro
 */
public class RegisterForm
{

	/** The title code. */
	private String titleCode;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The email. */
	private String email;

	/** The pwd. */
	private String pwd;

	/** The mobile number. */
	private String mobileNumber;

	/** The mobile country. */
	private String mobileCountry;

	/** The check pwd. */
	private String checkPwd;

	/** The consent form. */
	private ConsentForm consentForm;

	/** The terms check. */
	private boolean termsCheck;

	private boolean otpConsentCheck;

	/**
	 * Gets the title code.
	 *
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * Sets the title code.
	 *
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * Gets the pwd.
	 *
	 * @return the pwd
	 */
	public String getPwd()
	{
		return pwd;
	}

	/**
	 * Sets the pwd.
	 *
	 * @param pwd
	 *           the pwd to set
	 */
	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	/**
	 * Gets the check pwd.
	 *
	 * @return the checkPwd
	 */
	public String getCheckPwd()
	{
		return checkPwd;
	}

	/**
	 * Sets the check pwd.
	 *
	 * @param checkPwd
	 *           the checkPwd to set
	 */
	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	/**
	 * Gets the consent form.
	 *
	 * @return the consent form
	 */
	public ConsentForm getConsentForm()
	{
		return consentForm;
	}

	/**
	 * Sets the consent form.
	 *
	 * @param consentForm
	 *           the new consent form
	 */
	public void setConsentForm(final ConsentForm consentForm)
	{
		this.consentForm = consentForm;
	}

	/**
	 * Checks if is terms check.
	 *
	 * @return true, if is terms check
	 */
	public boolean isTermsCheck()
	{
		return termsCheck;
	}

	/**
	 * Sets the terms check.
	 *
	 * @param termsCheck
	 *           the new terms check
	 */
	public void setTermsCheck(final boolean termsCheck)
	{
		this.termsCheck = termsCheck;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber
	 *           the new mobile number
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the mobile country.
	 *
	 * @return the mobile country
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * Sets the mobile country.
	 *
	 * @param mobileCountry
	 *           the new mobile country
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the otpConsentCheck
	 */
	public boolean isOtpConsentCheck()
	{
		return otpConsentCheck;
	}

	/**
	 * @param otpConsentCheck
	 *           the otpConsentCheck to set
	 */
	public void setOtpConsentCheck(final boolean otpConsentCheck)
	{
		this.otpConsentCheck = otpConsentCheck;
	}


}
