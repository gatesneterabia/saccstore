/**
 *
 */
package com.sacc.saccstorefront.form.validation;



import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccstorefront.form.ThirdPartyProfileForm;
import com.sacc.saccuser.service.MobilePhoneService;


/**
 * @author monzer
 *
 */
@Component("thirdPartyProfileValidator")
public class ThirdPartyProfileValidator implements Validator
{

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return ThirdPartyProfileForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		if (object == null || !(object instanceof ThirdPartyProfileForm))
		{
			errors.rejectValue("profile", "thirdParty.profile.invalid");
		}
		final ThirdPartyProfileForm profile = (ThirdPartyProfileForm) object;
		final String titleCode = profile.getTitleCode();
		final String firstName = profile.getFirstName();
		final String lastName = profile.getLastName();
		final String email = profile.getEmail();
		final String mobileCountry = profile.getMobileCountry();
		final String mobileNumber = profile.getMobileNumber();

		if (StringUtils.isNotEmpty(titleCode) && StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "thirdParty.profile.titleCode.invalid");
		}

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "thirdParty.profile.firstName.invalid");
		}
		else if (StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "thirdParty.profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "thirdParty.profile.lastName.invalid");
		}
		else if (StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "thirdParty.profile.lastName.invalid");
		}

		if (StringUtils.isEmpty(mobileCountry))
		{
			errors.rejectValue("mobileCountry", "thirdParty.profile.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(mobileNumber) || !validateMobileNumberPattern(mobileNumber))
		{
			errors.rejectValue("mobileNumber", "thirdParty.profile.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(mobileCountry))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				profile.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				errors.rejectValue("mobileNumber", "thirdParty.profile.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
			}
		}

	}

	private boolean validateMobileNumberPattern(final String mobileNumber)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(mobileNumber);
		return matcher.matches();
	}

}
