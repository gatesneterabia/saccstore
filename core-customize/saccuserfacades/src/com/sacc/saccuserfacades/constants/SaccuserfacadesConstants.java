/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccuserfacades.constants;

/**
 * Global class for all Saccuserfacades constants. You can add global constants for your extension into this class.
 */
public final class SaccuserfacadesConstants extends GeneratedSaccuserfacadesConstants
{
	public static final String EXTENSIONNAME = "saccuserfacades";

	private SaccuserfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccuserfacadesPlatformLogo";
}
