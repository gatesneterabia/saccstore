
package schemas.dynamics.microsoft.page.item_web_service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Item_Web_Service_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Item_Web_Service_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Item_Web_Service" type="{urn:microsoft-dynamics-schemas/page/item_web_service}Item_Web_Service" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Item_Web_Service_List", propOrder = {
    "itemWebService"
})
public class ItemWebServiceList {

    @XmlElement(name = "Item_Web_Service", required = true)
    protected List<ItemWebService> itemWebService;

    /**
     * Gets the value of the itemWebService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemWebService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemWebService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemWebService }
     * 
     * 
     */
    public List<ItemWebService> getItemWebService() {
        if (itemWebService == null) {
            itemWebService = new ArrayList<ItemWebService>();
        }
        return this.itemWebService;
    }

}
