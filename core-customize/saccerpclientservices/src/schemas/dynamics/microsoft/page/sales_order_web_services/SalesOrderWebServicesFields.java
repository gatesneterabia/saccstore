
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Sales_Order_Web_Services_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Sales_Order_Web_Services_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="External_Document_No"/>
 *     &lt;enumeration value="Sell_to_Customer_No"/>
 *     &lt;enumeration value="Order_Date"/>
 *     &lt;enumeration value="Invoice_Discount_Calculation"/>
 *     &lt;enumeration value="Invoice_Discount_Value"/>
 *     &lt;enumeration value="Online_Status"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Sales_Order_Web_Services_Fields")
@XmlEnum
public enum SalesOrderWebServicesFields {

    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("External_Document_No")
    EXTERNAL_DOCUMENT_NO("External_Document_No"),
    @XmlEnumValue("Sell_to_Customer_No")
    SELL_TO_CUSTOMER_NO("Sell_to_Customer_No"),
    @XmlEnumValue("Order_Date")
    ORDER_DATE("Order_Date"),
    @XmlEnumValue("Invoice_Discount_Calculation")
    INVOICE_DISCOUNT_CALCULATION("Invoice_Discount_Calculation"),
    @XmlEnumValue("Invoice_Discount_Value")
    INVOICE_DISCOUNT_VALUE("Invoice_Discount_Value"),
    @XmlEnumValue("Online_Status")
    ONLINE_STATUS("Online_Status");
    private final String value;

    SalesOrderWebServicesFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SalesOrderWebServicesFields fromValue(String v) {
        for (SalesOrderWebServicesFields c: SalesOrderWebServicesFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
