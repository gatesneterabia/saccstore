
package schemas.dynamics.microsoft.page.payment_web_service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Payment_Web_Service_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Payment_Web_Service_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Customer_No"/>
 *     &lt;enumeration value="External_Document_No"/>
 *     &lt;enumeration value="Amount"/>
 *     &lt;enumeration value="Posting_Date"/>
 *     &lt;enumeration value="Bal_Account_Type"/>
 *     &lt;enumeration value="Bal_Account_No"/>
 *     &lt;enumeration value="Post"/>
 *     &lt;enumeration value="Online_Posted"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Payment_Web_Service_Fields")
@XmlEnum
public enum PaymentWebServiceFields {

    @XmlEnumValue("Customer_No")
    CUSTOMER_NO("Customer_No"),
    @XmlEnumValue("External_Document_No")
    EXTERNAL_DOCUMENT_NO("External_Document_No"),
    @XmlEnumValue("Amount")
    AMOUNT("Amount"),
    @XmlEnumValue("Posting_Date")
    POSTING_DATE("Posting_Date"),
    @XmlEnumValue("Bal_Account_Type")
    BAL_ACCOUNT_TYPE("Bal_Account_Type"),
    @XmlEnumValue("Bal_Account_No")
    BAL_ACCOUNT_NO("Bal_Account_No"),
    @XmlEnumValue("Post")
    POST("Post"),
    @XmlEnumValue("Online_Posted")
    ONLINE_POSTED("Online_Posted");
    private final String value;

    PaymentWebServiceFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentWebServiceFields fromValue(String v) {
        for (PaymentWebServiceFields c: PaymentWebServiceFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
