/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import java.util.Collection;
import java.util.Optional;

import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPModelService
{
	Optional<SaccERPClientWebServiceModel> find(final String code, final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model);

	Optional<SaccERPClientWebServiceModel> findFirst(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model);

	Collection<SaccERPClientWebServiceModel> findAll(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model);
}
