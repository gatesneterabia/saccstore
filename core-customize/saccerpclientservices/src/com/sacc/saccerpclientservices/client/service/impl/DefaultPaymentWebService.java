/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPPaymentWebService;
import com.sacc.saccerpclientservices.enums.SaccERPClientBalAccountType;
import com.sacc.saccerpclientservices.model.SaccERPClientPaymentWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;

import schemas.dynamics.microsoft.page.payment_web_service.BalAccountType;
import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebService;
import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultPaymentWebService extends DefaultWebService implements SaccERPPaymentWebService
{
	private static final Logger LOG = Logger.getLogger(DefaultPaymentWebService.class);

	protected void setBalAccountType(final PaymentWebService paymentWebService,
			final SaccERPClientPaymentWebServiceModel saccERPClientPaymentWebService)
	{
		final SaccERPClientBalAccountType balAccountType = saccERPClientPaymentWebService.getBalAccountType();
		if (SaccERPClientBalAccountType.G_L_ACCOUNT.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.G_L_ACCOUNT);
		}
		else if (SaccERPClientBalAccountType.CUSTOMER.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.CUSTOMER);
		}
		else if (SaccERPClientBalAccountType.VENDOR.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.VENDOR);
		}
		else if (SaccERPClientBalAccountType.BANK_ACCOUNT.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.BANK_ACCOUNT);
		}
		else if (SaccERPClientBalAccountType.FIXED_ASSET.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.FIXED_ASSET);
		}
		else if (SaccERPClientBalAccountType.IC_PARTNER.equals(balAccountType))
		{
			paymentWebService.setBalAccountType(BalAccountType.IC_PARTNER);
		}
	}

	protected PaymentWebService buildPaymentWebService(final String key, final String username, final String password)
			throws SaccERPWebServiceException
	{
		final PaymentWebService paymentWebService = new PaymentWebService();
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientPaymentWebServiceModel.class);
		if (model != null)
		{
			paymentWebService.setTestEnv(model.isTestEnvironment());
		}
		paymentWebService.setTimeout(getTimeout());
		paymentWebService.setKey(key);
		paymentWebService.setUsername(username);
		paymentWebService.setPassword(password);

		return paymentWebService;
	}

	protected PaymentWebService buildPaymentWebService(final ConsignmentModel consignment,
			final SaccERPClientPaymentWebServiceModel saccERPClientPaymentWebService) throws SaccERPWebServiceException
	{
		validateUser(consignment.getOrder().getUser());
		final OrderModel order = (OrderModel) consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();
		final PaymentWebService paymentWebService = buildPaymentWebService(order.getCode(),
				saccERPClientPaymentWebService.getUserName(), saccERPClientPaymentWebService.getPassword());

		paymentWebService.setCustomerNo(customer.getPk().toString());
		paymentWebService.setExternalDocumentNo("DOC");
		paymentWebService.setAmount(order.getTotalPrice() != null ? String.valueOf(order.getTotalPrice()) : "");
		paymentWebService.setPostingDate(convertDateToXMLGregorianCalendar(order.getCreationtime()));
		setBalAccountType(paymentWebService, saccERPClientPaymentWebService);
		paymentWebService.setBalAccountNo(saccERPClientPaymentWebService.getBalAccountNo());

		return paymentWebService;
	}

	protected void checkInstance(final SaccERPClientWebServiceModel model) throws SaccERPWebServiceException
	{
		if (!(model instanceof SaccERPClientPaymentWebServiceModel))
		{
			throw new SaccERPWebServiceException(
					String.format("Model not instance from[%]", SaccERPClientPaymentWebServiceModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}
	}

	protected SaccERPClientPaymentWebServiceModel getModel(final String modelCode) throws SaccERPWebServiceException
	{
		validate(modelCode, String.format("Invalid modelCode[%s]", modelCode), SaccERPExeptionType.BAD_REQUEST);

		final Optional<SaccERPClientWebServiceModel> result = getSaccERPModelService().find(modelCode, true,
				SaccERPClientPaymentWebServiceModel.class);
		validateOp(result, String.format("[%s] not found where code[%s]", SaccERPClientPaymentWebServiceModel._TYPECODE, modelCode),
				SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = result.get();
		checkInstance(model);
		validateCredentials(model.getUserName(), model.getPassword());

		return (SaccERPClientPaymentWebServiceModel) model;
	}

	@Override
	public List<PaymentWebService> readMultiple(final int size, final List<PaymentWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientPaymentWebServiceModel.class);
		final PaymentWebService paymentWebService = buildPaymentWebService("", model.getUserName(), model.getPassword());

		return paymentWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<PaymentWebService> readMultiple(final String username, final String password, final int size,
			final List<PaymentWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, "notReq");
		final PaymentWebService paymentWebService = buildPaymentWebService("", username, password);

		return paymentWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<PaymentWebService> readMultiple(final int size, final String modelCode,
			final List<PaymentWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientPaymentWebServiceModel model = getModel(modelCode);
		final PaymentWebService paymentWebService = buildPaymentWebService("", model.getUserName(), model.getPassword());

		return paymentWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<PaymentWebService> readMultiple(final int size, final SaccERPClientPaymentWebServiceModel model,
			final List<PaymentWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validate(model, String.format("Invalid [%s]", SaccERPClientPaymentWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		validateEssentialData(model.getUserName(), model.getPassword(), "notReq");
		final PaymentWebService paymentWebService = buildPaymentWebService("", model.getUserName(), model.getPassword());

		return paymentWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public PaymentWebService read(final String key) throws SaccERPWebServiceException
	{
		validate(key, "Invalid key", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientPaymentWebServiceModel.class);
		final PaymentWebService paymentWebService = buildPaymentWebService(key, model.getUserName(), model.getPassword());

		return paymentWebService.read();
	}

	@Override
	public PaymentWebService read(final String key, final String username, final String password) throws SaccERPWebServiceException
	{
		validate(key, "Invalid key", SaccERPExeptionType.BAD_REQUEST);
		validateCredentials(username, password);
		final PaymentWebService paymentWebService = buildPaymentWebService(key, username, password);

		return paymentWebService.read();
	}

	@Override
	public PaymentWebService read(final String key, final String modelCode) throws SaccERPWebServiceException
	{
		validate(key, "Invalid key", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientPaymentWebServiceModel model = getModel(modelCode);
		final PaymentWebService paymentWebService = buildPaymentWebService(key, model.getUserName(), model.getPassword());

		return paymentWebService.read();
	}

	@Override
	public PaymentWebService read(final String key, final SaccERPClientPaymentWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(key, "Invalid key", SaccERPExeptionType.BAD_REQUEST);
		validate(model, String.format("Invalid [%s]", SaccERPClientPaymentWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		final PaymentWebService paymentWebService = buildPaymentWebService(key, model.getUserName(), model.getPassword());

		return paymentWebService.read();
	}

	protected PaymentWebService executeUpdate(final PaymentWebService paymentWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP update paymentWebService request[%s]", marshal(paymentWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return paymentWebService.update();
	}

	protected PaymentWebService update(final PaymentWebService paymentWebService, SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(paymentWebService.getKey(), "Invalid key", SaccERPExeptionType.BAD_REQUEST);

		if (model == null)
		{
			model = findModel(SaccERPClientPaymentWebServiceModel.class);
			paymentWebService.setUsername(model.getUserName());
			paymentWebService.setPassword(model.getPassword());
			paymentWebService.setTestEnv(model.isTestEnvironment());
		}
		paymentWebService.setTimeout(getTimeout());
		return executeUpdate(paymentWebService);
	}

	@Override
	public PaymentWebService update(final PaymentWebService paymentWebService) throws SaccERPWebServiceException
	{
		validate(paymentWebService, "Invalid paymentWebService", SaccERPExeptionType.BAD_REQUEST);
		validate(paymentWebService.getKey(), "Invalid paymentWebService.key", SaccERPExeptionType.BAD_REQUEST);

		return update(paymentWebService, null);
	}

	@Override
	public PaymentWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SaccERPClientPaymentWebServiceModel model = getModel(modelCode);
		final PaymentWebService paymentWebService = buildPaymentWebService(consignment, model);

		return update(paymentWebService, model);
	}

	@Override
	public PaymentWebService update(final ConsignmentModel consignment, final SaccERPClientPaymentWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final PaymentWebService paymentWebService = buildPaymentWebService(consignment, model);

		return update(paymentWebService, model);
	}

	protected PaymentWebService executeCreate(final PaymentWebService paymentWebService) throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP create paymentWebService request[%s]", marshal(paymentWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientPaymentWebServiceModel.class);
		paymentWebService.setTestEnv(model.isTestEnvironment());
		paymentWebService.setTimeout(getTimeout());
		return paymentWebService.create();
	}

	@Override
	public PaymentWebService create(final PaymentWebService paymentWebService) throws SaccERPWebServiceException
	{
		validate(paymentWebService, "Invalid paymentWebService", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientPaymentWebServiceModel.class);
		paymentWebService.setUsername(model.getUserName());
		paymentWebService.setPassword(model.getPassword());

		return executeCreate(paymentWebService);
	}

	@Override
	public PaymentWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final PaymentWebService paymentWebService = buildPaymentWebService(consignment, getModel(modelCode));

		return executeCreate(paymentWebService);
	}

	@Override
	public PaymentWebService create(final ConsignmentModel consignment, final SaccERPClientPaymentWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final PaymentWebService paymentWebService = buildPaymentWebService(consignment, model);

		return executeCreate(paymentWebService);
	}
}