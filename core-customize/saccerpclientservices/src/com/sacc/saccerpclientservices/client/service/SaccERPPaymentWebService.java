/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientPaymentWebServiceModel;

import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebService;
import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPPaymentWebService
{
	PaymentWebService read(final String search) throws SaccERPWebServiceException;

	PaymentWebService read(final String search, final String username, final String password) throws SaccERPWebServiceException;

	PaymentWebService read(final String search, final String modelCode) throws SaccERPWebServiceException;

	PaymentWebService read(final String search, final SaccERPClientPaymentWebServiceModel model) throws SaccERPWebServiceException;

	PaymentWebService update(PaymentWebService t) throws SaccERPWebServiceException;

	PaymentWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	PaymentWebService update(final ConsignmentModel consignment, final SaccERPClientPaymentWebServiceModel model)
			throws SaccERPWebServiceException;

	PaymentWebService create(final PaymentWebService t) throws SaccERPWebServiceException;

	PaymentWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	PaymentWebService create(final ConsignmentModel consignment, final SaccERPClientPaymentWebServiceModel model)
			throws SaccERPWebServiceException;

	List<PaymentWebService> readMultiple(int size, final List<PaymentWebServiceFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException;

	List<PaymentWebService> readMultiple(final String username, final String password, final int size,
			final List<PaymentWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

	List<PaymentWebService> readMultiple(final int size, final String modelCode, final List<PaymentWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;

	List<PaymentWebService> readMultiple(final int size, final SaccERPClientPaymentWebServiceModel model,
			final List<PaymentWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;
}
