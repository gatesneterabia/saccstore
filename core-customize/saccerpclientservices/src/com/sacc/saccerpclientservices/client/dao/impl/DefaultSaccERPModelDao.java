/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.sacc.saccerpclientservices.client.dao.SaccERPModelDao;
import com.sacc.saccerpclientservices.client.factory.SaccERPModelFactory;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultSaccERPModelDao implements SaccERPModelDao
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Override
	public Optional<SaccERPClientWebServiceModel> find(final String code, final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		if (StringUtils.isNotBlank(code) && model != null)
		{
			final String typeCode = SaccERPModelFactory.getTypeCode(model);
			if (StringUtils.isNotBlank(typeCode))
			{
				final StringBuilder query = new StringBuilder(
						String.format("SELECT {%s} FROM {%s} WHERE {%s}=?%s", SaccERPClientWebServiceModel.PK, typeCode,
								SaccERPClientWebServiceModel.CODE, SaccERPClientWebServiceModel.CODE));

				final Map<String, Object> queryParams = new HashMap<String, Object>();
				queryParams.put(SaccERPClientWebServiceModel.CODE, code);

				if (active != null)
				{
					query.append(
							String.format(" AND {%s}=?%s", SaccERPClientWebServiceModel.ACTIVE, SaccERPClientWebServiceModel.ACTIVE));
					queryParams.put(SaccERPClientWebServiceModel.ACTIVE, active);
				}

				final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString(), queryParams);
				final SearchResult<SaccERPClientWebServiceModel> searchResult = getFlexibleSearchService().search(fQuery);
				if (searchResult != null && CollectionUtils.isNotEmpty(searchResult.getResult()))
				{
					return searchResult.getResult().stream().findFirst();
				}
			}
		}

		return Optional.empty();
	}

	@Override
	public Collection<SaccERPClientWebServiceModel> findAll(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		if (model != null)
		{
			final String typeCode = SaccERPModelFactory.getTypeCode(model);
			if (StringUtils.isNotBlank(typeCode))
			{
				final StringBuilder query = new StringBuilder(
						String.format(String.format("SELECT {%s} FROM {%s} WHERE 1=1", SaccERPClientWebServiceModel.PK, typeCode)));

				final Map<String, Object> queryParams = new HashMap<String, Object>();
				if (active != null)
				{
					query.append(
							String.format(" AND {%s}=?%s", SaccERPClientWebServiceModel.ACTIVE, SaccERPClientWebServiceModel.ACTIVE));
					queryParams.put(SaccERPClientWebServiceModel.ACTIVE, active);
				}

				final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString(), queryParams);
				final SearchResult<SaccERPClientWebServiceModel> searchResult = getFlexibleSearchService().search(fQuery);
				if (searchResult != null && CollectionUtils.isNotEmpty(searchResult.getResult()))
				{
					return searchResult.getResult();
				}
			}
		}

		return Collections.emptyList();
	}

	@Override
	public Optional<SaccERPClientWebServiceModel> findFirst(final Boolean active,
			final Class<? extends SaccERPClientWebServiceModel> model)
	{
		final Collection<SaccERPClientWebServiceModel> result = findAll(active, model);
		if (CollectionUtils.isNotEmpty(result))
		{
			return result.stream().findFirst();
		}

		return Optional.empty();
	}
}
