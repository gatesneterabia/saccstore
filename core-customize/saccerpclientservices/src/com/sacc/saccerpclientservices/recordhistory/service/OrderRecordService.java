/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

/**
 *
 */
public interface OrderRecordService
{
	OrderModel getOrderByCode(String orderCode);
}
