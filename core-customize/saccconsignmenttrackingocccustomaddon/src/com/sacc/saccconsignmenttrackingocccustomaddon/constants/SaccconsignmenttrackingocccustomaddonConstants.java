/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccconsignmenttrackingocccustomaddon.constants;

/**
 * Global class for all saccconsignmenttrackingocccustomaddon constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class SaccconsignmenttrackingocccustomaddonConstants extends GeneratedSaccconsignmenttrackingocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "saccconsignmenttrackingocccustomaddon"; //NOSONAR

	private SaccconsignmenttrackingocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
