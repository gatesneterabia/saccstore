/**
 *
 */
package com.sacc.saccstorecredit.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.sacc.saccstorecredit.enums.StoreCreditModeType;
import com.sacc.saccstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeService
{
	public StoreCreditModeModel getStoreCreditMode(String storeCreditModeTypeCode);

	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);

	public List<StoreCreditModeModel> getSupportedStoreCreditModes(BaseStoreModel baseStoreModel);

	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore();
}
