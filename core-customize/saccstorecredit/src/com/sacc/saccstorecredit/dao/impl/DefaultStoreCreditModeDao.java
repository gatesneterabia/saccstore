/**
 *
 */
package com.sacc.saccstorecredit.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import com.sacc.saccstorecredit.dao.StoreCreditModeDao;
import com.sacc.saccstorecredit.enums.StoreCreditModeType;
import com.sacc.saccstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public class DefaultStoreCreditModeDao extends AbstractItemDao implements StoreCreditModeDao
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sacc.saccstorecredit.dao.StoreCreditModeDao#getStoreCreditMode(com.sacc.saccstorecredit.enums.
	 * StoreCreditModeType)
	 */
	@Override
	public StoreCreditModeModel getStoreCreditMode(final StoreCreditModeType storeCreditModeType)
	{
		final String query = "SELECT {PK} FROM {StoreCreditMode} WHERE {storeCreditModeType}=?storeCreditModeType";

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		fQuery.addQueryParameter("storeCreditModeType", storeCreditModeType);

		final SearchResult<StoreCreditModeModel> result = getFlexibleSearchService().search(fQuery);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}


}
