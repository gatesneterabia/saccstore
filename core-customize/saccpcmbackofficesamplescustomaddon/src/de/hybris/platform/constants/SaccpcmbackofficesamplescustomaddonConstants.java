/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package de.hybris.platform.constants;

/**
 * Global class for all Saccpcmbackofficesamplescustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class SaccpcmbackofficesamplescustomaddonConstants extends GeneratedSaccpcmbackofficesamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "saccpcmbackofficesamplescustomaddon";

	private SaccpcmbackofficesamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
