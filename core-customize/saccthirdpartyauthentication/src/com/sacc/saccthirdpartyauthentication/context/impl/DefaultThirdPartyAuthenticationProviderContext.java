/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.sacc.saccthirdpartyauthentication.context.ThirdPartyAuthenticationProviderContext;
import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.strategy.AuthenticationProviderStrategy;


/**
 *
 */
public class DefaultThirdPartyAuthenticationProviderContext implements ThirdPartyAuthenticationProviderContext
{
	@Resource(name = "thirdPartyAuthenticationProviderMap")
	private Map<Class<?>, AuthenticationProviderStrategy> thirdPartyAuthenticationProviderMap;
	private static final String CMSSITE_NOT_FOUND = "cmsSite is null";

	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	protected Map<Class<?>, AuthenticationProviderStrategy> getThirdPartyAuthenticationProviderMap()
	{
		return thirdPartyAuthenticationProviderMap;
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getThirdPartyAuthenticationProviderByCurrentSite(
			final Class<?> providerClass)
	{

		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<AuthenticationProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentSite();
	}

	protected Optional<AuthenticationProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final AuthenticationProviderStrategy strategy = getThirdPartyAuthenticationProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getThirdPartyAuthenticationProvider(final CMSSiteModel cmsSiteModel,
			final Class<?> providerClass)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);

		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);

		final Optional<AuthenticationProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(cmsSiteModel);
	}
}
