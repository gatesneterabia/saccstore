package com.sacc.saccthirdpartyauthentication.service.impl;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.User;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.service.FacebookService;


public class DefaultFacebookService implements FacebookService
{
	private static final Logger LOG = Logger.getLogger(DefaultFacebookService.class);
	private static final String ATTRIBUTES = "name,first_name,last_name,email";

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("FacebookUserServiceImpl getData()");
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final FacebookClient facebookClient = new DefaultFacebookClient(token, appSecret, Version.LATEST);
		final User user = facebookClient.fetchObject("me", User.class, Parameter.with("fields", ATTRIBUTES));
		if (user == null)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final String firstName = user.getFirstName();
		final String lastName = user.getLastName();
		final String email = user.getEmail();
		final String name = user.getName();
		final String gender = user.getGender();
		final String id = user.getId();
		final String birthday = user.getBirthday();

		final ThirdPartyAuthenticationUserData userData = new ThirdPartyAuthenticationUserData();
		userData.setId(id);
		userData.setFirstName(firstName);
		userData.setLastName(lastName);
		userData.setName(name);
		userData.setEmail(email);
		userData.setDateOfBirth(birthday);

		return Optional.ofNullable(userData);
	}

	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(token, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}
