/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.constants;

/**
 * Global class for all Saccthirdpartyauthentication constants. You can add global constants for your extension into this class.
 */
public final class SaccthirdpartyauthenticationConstants extends GeneratedSaccthirdpartyauthenticationConstants
{
	public static final String EXTENSIONNAME = "saccthirdpartyauthentication";

	private SaccthirdpartyauthenticationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccthirdpartyauthenticationPlatformLogo";
}
