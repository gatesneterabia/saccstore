package com.sacc.saccthirdpartyauthentication.exception;

import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;


public class ThirdPartyAuthenticationException extends Exception
{

	private final ThirdPartyAuthenticationExceptionType type;

	public ThirdPartyAuthenticationException(final ThirdPartyAuthenticationExceptionType type, final String msg)
	{
		super(msg);
		this.type = type;
	}

	public ThirdPartyAuthenticationExceptionType getType()
	{
		return type;
	}



}
