/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.strategy.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterAuthenticatonData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterCredinatial;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.model.TwitterAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.service.TwitterService;
import com.sacc.saccthirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;


/**
 *
 */
public class DefaultTwitterThirdPartyAuthenticationStrategy implements ThirdPartyAuthenticationStrategy
{
	@Resource(name = "twitterService")
	private TwitterService twitterService;


	@Override
	public Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof TwitterAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}


		final ThirdPartyAuthenticationProviderData thirdPartyAuthenticationProviderData = new ThirdPartyAuthenticationProviderData();

		thirdPartyAuthenticationProviderData.setType(ThirdPartyAuthenticationType.TWITTER.getCode());

		return Optional.ofNullable(thirdPartyAuthenticationProviderData);

	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof TwitterAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final TwitterAuthenticationProviderModel twitterProvider = (TwitterAuthenticationProviderModel) provider;

		if (!(data instanceof TwitterAuthenticatonData))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final TwitterAuthenticatonData twitterCredinatial = (TwitterAuthenticatonData) data;
		final String consumerKey = twitterCredinatial.getConsumerKey();
		final String consumerSecret = twitterCredinatial.getConsumerSecret();
		final String oauthToken = twitterCredinatial.getOauthToken();
		final String oauthTokenSecret = twitterCredinatial.getOauthTokenSecret();
		final String oauthVerifier = twitterCredinatial.getOauthVerifier();

		final Optional<ThirdPartyAuthenticationUserData> userData = twitterService.getData(oauthVerifier, consumerKey,
				consumerSecret, oauthToken, oauthTokenSecret);

		if (!userData.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}
		return userData;
	}

	@Override
	public Optional<TwitterFormData> getFormData(final Object data, final ThirdPartyAuthenticationProviderModel provider,
			final String callbackUrl)
			throws ThirdPartyAuthenticationException
	{
		if (!(data instanceof TwitterCredinatial))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final TwitterCredinatial twitterCredinatial = (TwitterCredinatial) data;
		final String consumerKey = twitterCredinatial.getConsumerKey();
		final String consumerSecret = twitterCredinatial.getConsumerSecret();

		final TwitterAuthenticationProviderModel twitterProvider = (TwitterAuthenticationProviderModel) provider;

		final String fullCallbackUrl = callbackUrl + twitterProvider.getCallbackUrl();

		final Optional<TwitterFormData> userData = twitterService.getTwitterFormData(consumerKey, consumerSecret, fullCallbackUrl);

		return userData;
	}

	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object data, final Object token,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || token == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof TwitterAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final TwitterAuthenticationProviderModel twitterProvider = (TwitterAuthenticationProviderModel) provider;

		final String bearerToken = twitterProvider.getBearerToken();

		return twitterService.verifyThirdPartyAccessToken(twitterProvider.getConsumerKey(), twitterProvider.getConsumerSecret(),
				String.valueOf(data), bearerToken);

	}

}


