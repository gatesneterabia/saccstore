/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccconsignmenttrackingcustomaddon.constants;

/**
 * Global class for all Saccconsignmenttrackingcustomaddonaddon constants. You can add global constants for your extension into this class.
 */
public final class SaccconsignmenttrackingcustomaddonaddonConstants extends GeneratedSaccconsignmenttrackingcustomaddonaddonConstants
{
	public static final String EXTENSIONNAME = "saccconsignmenttrackingcustomaddon";

	private SaccconsignmenttrackingcustomaddonaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
