package com.sacc.sacccustomersupportbackoffice.actions.returns;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.util.notifications.NotificationService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomCreateReturnRequestAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{
	private static final Logger LOG = Logger.getLogger(CustomCreateReturnRequestAction.class);
	protected static final String SOCKET_OUT_CONTEXT = "createReturnRequestContext";
	protected static final String CREATE_RETURN_REQUEST_MODIFIED_FAILURE = "customersupport.create.returnrequest.modified.failure";
	@Resource
	private ReturnService returnService;
	@Resource
	private ModelService modelService;
	@Resource
	private NotificationService notificationService;

	public boolean canPerform(final ActionContext<OrderModel> actionContext)
	{
		final OrderModel order = actionContext.getData();
		return containsConsignments(order) && isConsignmentsAllowedForReturn(order)
				&& !getReturnService().getAllReturnableEntries(order).isEmpty() && !order.isSyncPartialCancelFailed();
	}

	private boolean containsConsignments(final OrderModel order)
	{
		return order != null && !CollectionUtils.isEmpty(order.getEntries()) && !CollectionUtils.isEmpty(order.getConsignments());
	}

	private boolean isConsignmentsAllowedForReturn(final OrderModel order)
	{
		return order.getConsignments().stream().filter(Objects::nonNull)
				.anyMatch(c -> isConsignmentStatusApplicableForReturn(c.getStatus()));
	}

	private boolean isConsignmentStatusApplicableForReturn(final ConsignmentStatus status)
	{
		return ConsignmentStatus.SHIPPED.equals(status) || ConsignmentStatus.DELIVERY_COMPLETED.equals(status)
				|| ConsignmentStatus.PICKUP_COMPLETE.equals(status);
	}

	public String getConfirmationMessage(final ActionContext<OrderModel> actionContext)
	{
		return null;
	}

	public boolean needsConfirmation(final ActionContext<OrderModel> actionContext)
	{
		return false;
	}

	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> actionContext)
	{
		ActionResult actionResult;
		final OrderModel order = actionContext.getData();
		getModelService().refresh(order);
		if (canPerform(actionContext))
		{
			sendOutput(SOCKET_OUT_CONTEXT, actionContext.getData());
			actionResult = new ActionResult(ActionResult.SUCCESS);
		}
		else
		{
			notificationService.notifyUser(notificationService.getWidgetNotificationSource(actionContext), "JustMessage",
					NotificationEvent.Level.FAILURE, new Object[]
					{ actionContext.getLabel(CREATE_RETURN_REQUEST_MODIFIED_FAILURE) });
			actionResult = new ActionResult(ActionResult.ERROR);
		}
		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_PERSISTED);
		return actionResult;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected ReturnService getReturnService()
	{
		return returnService;
	}
}
