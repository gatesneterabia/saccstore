/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwishlist.constants;

/**
 * Global class for all Saccwishlist constants. You can add global constants for your extension into this class.
 */
public final class SaccwishlistConstants extends GeneratedSaccwishlistConstants
{
	public static final String EXTENSIONNAME = "saccwishlist";

	private SaccwishlistConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccwishlistPlatformLogo";
}
